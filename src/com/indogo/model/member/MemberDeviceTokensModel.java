package com.indogo.model.member;

import java.sql.Timestamp;

public class MemberDeviceTokensModel {
	public long member_id;
	public String token;
	public Timestamp create_time;
}
