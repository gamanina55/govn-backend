package com.indogo.model.member;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.indogo.MemberSex;
import com.indogo.MemberStatus;

public class MemberModel {
	public long member_id;
	public String member_name;
	public String member_login_id;
	public String phone_no;
	public String arc_no;
	public Timestamp arc_expire_date;
	public String address;
	public Timestamp birthday;
	public String identity_card;
	public String passport;
	public String nationality_cname;
	public String permanent_address;
	public Timestamp lm_time;
	public String lm_user;
	public String arc_photo_basename;
	public String arc_back_photo_basename;
	public String signature_photo_basename;
	public String email;
	public MemberStatus status;
	public boolean signature_need_fix;
	public MemberSex sex;
	public String app_phone_no;
	public String id_card_photo_basename;
	public String self_photo_basename;
	public String app_arc_image;
	public String app_arc_back_image;
	public String app_signature_image;
	public String app_id_card_image;
	public String app_self_image;
	public int is_new;
	public List<String> memberDeviceTokens = new ArrayList<String>();

	public void addMemberDeviceToken(String token) {
		if (this.memberDeviceTokens.indexOf(token) == -1) {
			this.memberDeviceTokens.add(token);
		}
	}
}
