package com.indogo.model.member;

import java.sql.Timestamp;

public class MoneyTransferIsBanModel {
	public long txn_id;
	public Timestamp lm_time;
	public String lm_user;
	public int old_is_ban;
	public int new_is_ban;
}
