package com.indogo.relay.config;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;

import com.indogo.util.Image;
import com.lionpig.webui.database.ResultSetWrapperStringify;
import com.lionpig.webui.http.IFunction;
import com.lionpig.webui.http.func.TablePage;
import com.lionpig.webui.http.struct.FunctionItem;
import com.lionpig.webui.http.tablepage.ITablePage;
import com.lionpig.webui.http.tablepage.TablePageColumn;
import com.lionpig.webui.http.tablepage.TablePageFilter;
import com.lionpig.webui.http.tablepage.TablePageRowAttribute;
import com.lionpig.webui.http.tablepage.TablePageSort;
import com.lionpig.webui.http.util.C;
import com.lionpig.webui.http.util.Helper;
import com.lionpig.webui.http.util.Stringify;

import java.io.InputStream;
import java.sql.Connection;

public class AppPhotoConfiguration implements IFunction, ITablePage {

	@Override
	public String getTableOwner() {
		return null;
	}

	@Override
	public String getTableName() {
		return C.app_photo_list;
	}

	@Override
	public List<TablePageColumn> getColumns(FunctionItem fi) {
		List<TablePageColumn> cols = new ArrayList<TablePageColumn>();
		cols.add(new TablePageColumn(C.id, C.columnTypeNumber, C.columnDirectionDefault, true, false, "ID"));
		cols.add(new TablePageColumn(C.photo, C.columnTypeString, C.columnDirectionDefault, true, false, "Photo"));
		cols.add(new TablePageColumn(C.seq_no, C.columnTypeNumber, C.columnDirectionDefault, true, false, "Seq NO"));
		cols.add(new TablePageColumn(C.status, C.columnTypeString, C.columnDirectionDefault, true, false, "Status"));
		return cols;
	}

	@Override
	public void populateRowData(FunctionItem fi, Hashtable<String, TablePageColumn> cols, ResultSetWrapperStringify r, boolean isHtml,
			TablePageRowAttribute rowAttr) throws Exception {
		String photo = null;
		if(!Helper.isNullOrEmpty(r.getString(C.photo))) {
			photo = String.format("<img src='%s' />", formatPhotoUrl(fi, r.getString(C.photo)));
		}
				
		String status = Integer.parseInt(r.getInt(C.status)) == 1 ? "啟用" : "停用";

		cols.get(C.id).setValue(r.getInt(C.id));
		cols.get(C.photo).setValue(photo);
		cols.get(C.seq_no).setValue(r.getInt(C.seq_no));
		cols.get(C.status).setValue(status);
	}

	@Override
	public boolean populateRowDataBefore(FunctionItem fi, List<TablePageColumn> column, List<TablePageSort> sort,
			List<TablePageFilter> filter) throws Exception {
		return false;
	}

	@Override
	public void populateRowDataAfter(FunctionItem fi) throws Exception {
	}

	public static String formatPhotoUrl(FunctionItem fi, String photo_basename) throws Exception {
		String photoUrl = fi.getConnection().getGlobalConfig(C.app_photo, C.image_base_url);
		StringBuilder sb = new StringBuilder();
		sb.append(photoUrl).append("/").append(photo_basename).append(".png");
		return sb.toString();
	}
	
	public String execute(HttpServletRequest req, HttpServletResponse resp,
			FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		String action = Helper.getString(params, C.action, true);
		
		if (action.equals(C.insert)) {
			return insert(fi);
		} else if (action.equals(C.update)) {
			return update(fi);
		} else if (action.equals(C.delete)) {
			return delete(fi);
		} else if(action.contentEquals(C.photo_upload)) {
			Hashtable<String, FileItem> uploadFiles = fi.getUploadedFiles();
			if (uploadFiles.size() > 0) {
				FileItem photo = uploadFiles.get(C.photo_upload);
				if (photo != null && photo.getName().length() > 0) {
					String photo_basename = Helper.getString(params, C.photo_basename, false);
					photo_basename = saveImage(fi, photo.getInputStream(), fi.getConnection().getGlobalConfig(C.app_photo, C.image_base_directory), photo_basename);

					String baseUrl = fi.getConnection().getGlobalConfig(C.app_photo, C.image_base_url);
					StringBuilder sb = new StringBuilder();
					sb.append(photo_basename).append(C.char_31)
					.append(baseUrl).append("/").append(photo_basename).append(".png");
					return sb.toString();
				} else
					throw new Exception("input [photo_upload] cannot be emtpy");
			} else
				throw new Exception("input [photo_upload] cannot be emtpy");
		} else if (action.equals(C.getDataForUpdate)) {
			int id = Helper.getInt(params, "id", true);
			return getDataForUpdate(fi, id);
		} else {
			throw new Exception(String.format(C.unknown_action, action));
		}
	}

	public String getDataForUpdate(FunctionItem fi, int id) throws Exception {
		StringBuilder sb = new StringBuilder();
		AppPhotoModel model = this.getData(fi, id);

		sb.append(model.id).append(C.char_31)
		.append(Stringify.getString(model.photo)).append(C.char_31)
		.append(model.seq_no).append(C.char_31)
		.append(model.status).append(C.char_31)
		.append(formatPhotoUrl(fi, model.photo));
		return sb.toString();
	}
	
	public AppPhotoModel getData(FunctionItem fi, int id) throws Exception {
		PreparedStatement pstmt = fi.getConnection().getConnection().prepareStatement("SELECT `id`, `photo`, `seq_no`, `status` FROM `app_photo_list` WHERE `id` = ?");
		try {
			pstmt.setInt(1, id);
			try (ResultSet r = pstmt.executeQuery()) {
				if (r.next()) {
					AppPhotoModel model = new AppPhotoModel();
					model.id = r.getInt(1);
					model.photo = r.getString(2);
					model.seq_no = r.getInt(3);
					model.status = r.getInt(4);
					return model;
				} else {
					throw new Exception(String.format(C.data_not_exist, "id = " + id));
				}
			}
		} finally {
			pstmt.close();
		}
	}
	
	private String insert(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		
		AppPhotoModel model = new AppPhotoModel();
		model.photo = Helper.getString(params, C.photo_basename, true, C.photo);
		model.seq_no = Helper.getInt(params, C.seq_no, false, C.seq_no);
		model.status = Helper.getInt(params, C.status, false, C.status);
		
		Connection conn = fi.getConnection().getConnection();
		try {
			insert(fi, model);

			List<TablePageFilter> filter = new ArrayList<TablePageFilter>();
			filter.add(new TablePageFilter(C.id, C.columnTypeNumber, C.operationEqual, String.valueOf(model.id), null));
			TablePage p = new TablePage();
			String s = p.getRows(this, fi, 1, 1, this.getColumns(fi), null, filter, null, null, null);
						
			conn.commit();
			
			return s;
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	private String update(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		
		AppPhotoModel model = new AppPhotoModel();
		model.id = Helper.getInt(params, "id", true, "id");
		model.photo = Helper.getString(params, C.photo_basename, true, C.photo);
		model.seq_no = Helper.getInt(params, C.seq_no, false, C.seq_no);
		model.status = Helper.getInt(params, C.status, false, C.status);
		
		Connection conn = fi.getConnection().getConnection();
		try {
			update(fi, model);

			List<TablePageFilter> filter = new ArrayList<TablePageFilter>();
			filter.add(new TablePageFilter("id", "NUMBER", "=", String.valueOf(model.id), null));
			TablePage p = new TablePage();
			String s = p.getRows(this, fi, 1, 1, this.getColumns(fi), null, filter, null, null, null);
			
			conn.commit();
			
			return s;
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	private String delete(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		AppPhotoModel model = new AppPhotoModel();
		model.id = Helper.getInt(params, "id", true, "id");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			delete(fi, model);
			conn.commit();		
			return "1";
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	public void insert(FunctionItem fi, AppPhotoModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("INSERT INTO `app_photo_list` (`photo`, `seq_no`, `status`) values (?, ?, ?)");
		PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM `app_photo_list` WHERE `photo` = ?");
		ResultSet r;
		try {
			pstmt.setString(1, model.photo);
			pstmt.setInt(2, model.seq_no);
			pstmt.setInt(3, model.status);
			pstmt.executeUpdate();
			
			pstmt2.setString(1,  model.photo);
			r = pstmt2.executeQuery();
			
			try {
				if (r.next()) {
					model.id = r.getInt("id");
				}
			}
			finally {
				r.close();
			}	
			
		} finally {
			pstmt.close();
			pstmt2.close();
		}
	}
	
	public void update(FunctionItem fi, AppPhotoModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmtLock = conn.prepareStatement("SELECT `id`, `photo`, `seq_no`, `status` FROM `app_photo_list` WHERE `id` = ? for update");
		PreparedStatement pstmtUpdate = conn.prepareStatement("UPDATE `app_photo_list` SET `photo` = ?, `seq_no` = ?, `status` = ? WHERE `id` = ?");
		try {
			String photo;
			pstmtLock.setInt(1, model.id);
			ResultSet r = pstmtLock.executeQuery();
			try {
				if (r.next()) {
					photo = r.getString(2);
				} else {
					throw new Exception(String.format(C.data_not_exist, "id = " + model.id));
				}
			} finally {
				r.close();
			}
					
			pstmtUpdate.setString(1, model.photo);
			pstmtUpdate.setInt(2, model.seq_no);
			pstmtUpdate.setInt(3, model.status);
			pstmtUpdate.setInt(4, model.id);
			pstmtUpdate.executeUpdate();
			
		} finally {
			pstmtLock.close();
			pstmtUpdate.close();
		}
	}
	
	public void delete(FunctionItem fi, AppPhotoModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		String remoteUri = fi.getConnection().getGlobalConfig(C.app_photo, C.image_base_directory);
		PreparedStatement pstmtLock = conn.prepareStatement("SELECT `id`, `photo`, `seq_no`, `status` FROM `app_photo_list` WHERE `id` = ? for update");
		PreparedStatement pstmtDelete = conn.prepareStatement("DELETE FROM `app_photo_list` WHERE `id` = ?");
		try {
			String photo;
			pstmtLock.setInt(1, model.id);
			ResultSet r = pstmtLock.executeQuery();
			try {
				if (r.next()) {
					photo = r.getString(2);
				} else {
					throw new Exception(String.format(C.data_not_exist, "id = " + model.id));
				}
			} finally {
				r.close();
			}
					
			pstmtDelete.setInt(1, model.id);
			pstmtDelete.executeUpdate();
			
		} finally {
			pstmtLock.close();
			pstmtDelete.close();
		}
	}
	
	public List<AppPhotoModel> getAppPhotoList(FunctionItem fi) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		Statement stmt = conn.createStatement();
		try {
			try (ResultSetWrapperStringify r = new ResultSetWrapperStringify(stmt.executeQuery("SELECT `id`, `photo`, `seq_no`, `status` FROM `app_photo_list` ORDER BY `seq_no`, `id` ASC"))) {
				List<AppPhotoModel> list = new ArrayList<>();
				while (r.next()) {
					AppPhotoModel m = new AppPhotoModel();
					m.id = Integer.parseInt(r.getInt(1));
					m.photo = r.getString(2);
					m.seq_no = Integer.parseInt(r.getInt(3));
					m.status = Integer.parseInt(r.getInt(4));
					list.add(m);
				}
				return list;
			}
		} finally {
			stmt.close();
		}
	}

	private String saveImage(FunctionItem fi, InputStream imageSource, String remoteUri, String deleteOldFilename) throws Exception {
		String baseName = UUID.randomUUID().toString().replaceAll("-", "");
		String filename = String.format("%s.png", baseName);
		String deleteFilePath = null;
		if(!Helper.isNullOrEmpty(deleteOldFilename)) {
			deleteFilePath = String.format("%s/%s.png", remoteUri, deleteOldFilename);
		}
		
		int width = Integer.valueOf(fi.getConnection().getGlobalConfig(C.app_photo, "photo_resize_width"));	
		String format = fi.getConnection().getGlobalConfig(C.app_photo, "photo_format");
		return Image.saveImage(fi, imageSource, baseName, remoteUri, deleteFilePath, filename, width, format);
	}
}
