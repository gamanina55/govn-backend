package com.indogo.relay.config;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.indogo.relay.config.BanBankAccountModel;
import com.lionpig.webui.database.ResultSetWrapperStringify;
import com.lionpig.webui.http.IFunction;
import com.lionpig.webui.http.func.TablePage;
import com.lionpig.webui.http.struct.FunctionItem;
import com.lionpig.webui.http.tablepage.ITablePage;
import com.lionpig.webui.http.tablepage.TablePageColumn;
import com.lionpig.webui.http.tablepage.TablePageFilter;
import com.lionpig.webui.http.tablepage.TablePageRowAttribute;
import com.lionpig.webui.http.tablepage.TablePageSort;
import com.lionpig.webui.http.util.C;
import com.lionpig.webui.http.util.Helper;

import java.sql.Connection;

public class BanBankAccountConfiguration implements IFunction, ITablePage {

	private HashMap<String, String> bankCodeList = new HashMap<>();
	
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp,
			FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		String action = Helper.getString(params, C.action, true);
		
		if (action.equals(C.insert))
			return this.insert(fi);
		else if (action.equals(C.update))
			return this.update(fi);
		else if (action.equals(C.delete))
			return this.delete(fi);
		else if (action.equals(C.getDataForUpdate)) {
			BanBankAccountModel m = this.getDataForUpdate(fi, this, Helper.getInt(params, "id", true));

			StringBuilder sb = new StringBuilder();
			sb.append(m.id).append(C.char_31)
			  .append(Helper.replaceNull(m.bank_code)).append(C.char_31)
			  .append(Helper.replaceNull(m.account_name));
			
			return sb.toString();
		} else if (action.equals(C.init)) {
			return this.getBankCodeList(fi);
		} else
			throw new Exception(String.format(C.unknown_action, action));
	}

	@Override
	public String getTableOwner() {
		return null;
	}

	@Override
	public String getTableName() {
		return "ban_bank_account_list";
	}

	@Override
	public List<TablePageColumn> getColumns(FunctionItem fi) {
		
		List<TablePageColumn> cols = new ArrayList<TablePageColumn>();
		cols.add(new TablePageColumn(C.id, C.columnTypeNumber, C.columnDirectionDefault, true, false, C.id, true, false));
		cols.add(new TablePageColumn(C.bank_code, C.columnTypeString, C.columnDirectionDefault, true, false, C.bank_code, true, false));
		cols.add(new TablePageColumn(C.bank_name, C.columnTypeString, C.columnDirectionDefault, false, true, C.bank_name));
		cols.add(new TablePageColumn(C.account_name, C.columnTypeString, C.columnDirectionDefault, true, false, C.account_name, true, false));
		return cols;
	}

	@Override
	public void populateRowData(FunctionItem fi,
			Hashtable<String, TablePageColumn> cols, ResultSetWrapperStringify r,
			boolean isHtml, TablePageRowAttribute rowAttr) throws Exception {	
		String bank_code = r.unwrap().unwrap().getString(C.bank_code);
		cols.get(C.id).setValue(r.getInt(C.id));
		cols.get(C.bank_code).setValue(r.getString(C.bank_code));
		cols.get(C.bank_name).setValue(bankCodeList.get(bank_code));
		cols.get(C.account_name).setValue(r.getString(C.account_name));
	}

	@Override
	public boolean populateRowDataBefore(FunctionItem fi,
			List<TablePageColumn> column, List<TablePageSort> sort,
			List<TablePageFilter> filter) throws Exception {
		Statement stmt = fi.getConnection().getConnection().createStatement();
		try {
			ResultSet r = stmt.executeQuery("SELECT `bank_code`, `bank_name` FROM `bank_code_list`");
			try {
				while (r.next()) {
					String bank_code = r.getString(1);
					String bank_name = r.getString(2);
					bankCodeList.put(bank_code, bank_name);
				}
			} finally {
				r.close();
			}
		} finally {
			stmt.close();
		}
		return false;
	}

	@Override
	public void populateRowDataAfter(FunctionItem fi) throws Exception {
	}
	
	private String insert(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		
		BanBankAccountModel model = new BanBankAccountModel();
		model.bank_code = Helper.getString(params, "bank_code", true, "bank_code");
		model.account_name = Helper.getString(params, "account_name", true, "account_name");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			insert(fi, model);

			List<TablePageFilter> filter = new ArrayList<TablePageFilter>();
			filter.add(new TablePageFilter(C.id, C.columnTypeNumber, C.operationEqual, String.valueOf(model.id), null));
			TablePage p = new TablePage();
			String s = p.getRows(this, fi, 1, 1, this.getColumns(fi), null, filter, null, null, null);
			
			conn.commit();
			
			return s;
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	private String update(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		
		BanBankAccountModel model = new BanBankAccountModel();
		model.id = Helper.getInt(params, "id", true, "id");
		model.bank_code = Helper.getString(params, "bank_code", true, "bank_code");
		model.account_name = Helper.getString(params, "account_name", true, "account_name");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			update(fi, model);

			List<TablePageFilter> filter = new ArrayList<TablePageFilter>();
			filter.add(new TablePageFilter("id", "NUMBER", "=", String.valueOf(model.id), null));
			TablePage p = new TablePage();
			String s = p.getRows(this, fi, 1, 1, this.getColumns(fi), null, filter, null, null, null);
			
			conn.commit();
			
			return s;
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	private String delete(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		BanBankAccountModel model = new BanBankAccountModel();
		model.id = Helper.getInt(params, "id", true, "id");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			delete(fi, model);
			conn.commit();
			return "1";
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}

	public void insert(FunctionItem fi, BanBankAccountModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("INSERT INTO `ban_bank_account_list` (`bank_code`, `account_name`) VALUES (?,?)");
		PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM `ban_bank_account_list` WHERE `bank_code` = ? AND `account_name` = ?");
		ResultSet r;
		try {
			pstmt.setString(1, model.bank_code);
			pstmt.setString(2, model.account_name);
			pstmt.executeUpdate();
			
			pstmt2.setString(1, model.bank_code);
			pstmt2.setString(2, model.account_name);
			r = pstmt2.executeQuery();
			
			try {
				if (r.next()) {
					model.id = r.getInt("id");
				}
			}
			finally {
				r.close();
			}		
		}
		finally {
			pstmt.close();
			pstmt2.close();
		}
	}

	public void update(FunctionItem fi, BanBankAccountModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("UPDATE `ban_bank_account_list` SET `bank_code` = ?, `account_name` = ? WHERE id = ?");
		try {
			pstmt.setString(1, model.bank_code);
			pstmt.setString(2, model.account_name);
			pstmt.setInt(3, model.id);
			pstmt.executeUpdate();
		}
		finally {
			pstmt.close();
		}
	}
	
	public void delete(FunctionItem fi, BanBankAccountModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("DELETE FROM `ban_bank_account_list` where id = ?");
		try {
			pstmt.setInt(1, model.id);
			pstmt.executeUpdate();
		}
		finally {
			pstmt.close();
		}
	}
	
	public BanBankAccountModel getDataForUpdate(FunctionItem fi, ITablePage page, int id) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM `ban_bank_account_list` WHERE `id` = ?");
		ResultSet r;
		try {
			pstmt.setInt(1, id);
			r = pstmt.executeQuery();
			try {
				if (r.next()) {
					BanBankAccountModel m = new BanBankAccountModel();
					m.id = id;
					m.bank_code = r.getString("bank_code");
					m.account_name = r.getString("account_name");
					return m;
				}
				else
					throw new Exception("id [" + id + "] not exist");
			}
			finally {
				r.close();
			}
		}
		finally {
			pstmt.close();
		}
	}
	
	private String getBankCodeList(FunctionItem fi) throws Exception {
		Statement stmt = fi.getConnection().getConnection().createStatement();
		try {
			StringBuilder sb = new StringBuilder();
			try (ResultSetWrapperStringify r = new ResultSetWrapperStringify(stmt.executeQuery("SELECT `bank_code`, `bank_name` FROM `bank_code_list` ORDER BY `bank_code` ASC"))) {
				int index = 0;
				while (r.next()) {
					String bank_code = r.getString(1); 
					String bank_name = r.getString(2);
					
					if(index == 0) {
						sb.append("<option value=\"").append(bank_code).append("\" selected>").append(bank_name).append("</option>");	
					} else {
						sb.append("<option value=\"").append(bank_code).append("\">").append(bank_name).append("</option>");
					}
					index += 1;
				}
			}
			return sb.toString();
		} finally {
			stmt.close();
		}
	}

}
