package com.indogo.relay.config;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.indogo.relay.config.BanPhoneModel;
import com.lionpig.webui.database.ResultSetWrapperStringify;
import com.lionpig.webui.http.IFunction;
import com.lionpig.webui.http.func.TablePage;
import com.lionpig.webui.http.struct.FunctionItem;
import com.lionpig.webui.http.tablepage.ITablePage;
import com.lionpig.webui.http.tablepage.TablePageColumn;
import com.lionpig.webui.http.tablepage.TablePageFilter;
import com.lionpig.webui.http.tablepage.TablePageRowAttribute;
import com.lionpig.webui.http.tablepage.TablePageSort;
import com.lionpig.webui.http.util.C;
import com.lionpig.webui.http.util.Helper;

import java.sql.Connection;

public class BanPhoneConfiguration implements IFunction, ITablePage {

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp,
			FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		String action = Helper.getString(params, C.action, true);
		
		if (action.equals(C.insert))
			return this.insert(fi);
		else if (action.equals(C.update))
			return this.update(fi);
		else if (action.equals(C.delete))
			return this.delete(fi);
		else if (action.equals(C.getDataForUpdate)) {
			BanPhoneModel m = this.getDataForUpdate(fi, this, Helper.getInt(params, "id", true));

			StringBuilder sb = new StringBuilder();
			sb.append(Helper.replaceNull(m.phone_no)).append(C.char_31)
			  .append(Helper.replaceNull(m.description));
			
			return sb.toString();
		} else
			throw new Exception(String.format(C.unknown_action, action));
	}

	@Override
	public String getTableOwner() {
		return null;
	}

	@Override
	public String getTableName() {
		return "ban_phone_list";
	}

	@Override
	public List<TablePageColumn> getColumns(FunctionItem fi) {
		List<TablePageColumn> cols = new ArrayList<TablePageColumn>();
		cols.add(new TablePageColumn(C.id, C.columnTypeNumber, C.columnDirectionDefault, true, false, C.id, true, false));
		cols.add(new TablePageColumn(C.phone_no, C.columnTypeString, C.columnDirectionDefault, true, false, C.phone_no));
		cols.add(new TablePageColumn(C.description, C.columnTypeString, C.columnDirectionDefault, true, false, C.description));
		cols.add(new TablePageColumn(C.updated, C.columnTypeDateTime, C.columnDirectionDefault, true, false, C.updated));
		return cols;
	}

	@Override
	public void populateRowData(FunctionItem fi,
			Hashtable<String, TablePageColumn> cols, ResultSetWrapperStringify r,
			boolean isHtml, TablePageRowAttribute rowAttr) throws Exception {
		cols.get(C.id).setValue(r.getInt(C.id));
		cols.get(C.phone_no).setValue(r.getString(C.phone_no));
		cols.get(C.description).setValue(r.getString(C.description));
		cols.get(C.updated).setValue(r.getTimestamp(C.updated));
	}

	@Override
	public boolean populateRowDataBefore(FunctionItem fi,
			List<TablePageColumn> column, List<TablePageSort> sort,
			List<TablePageFilter> filter) throws Exception {
		return false;
	}

	@Override
	public void populateRowDataAfter(FunctionItem fi) throws Exception {
	}
	
	private String insert(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		
		BanPhoneModel model = new BanPhoneModel();
		model.phone_no = Helper.getString(params, "phone_no", true, "phone_no");
		model.description = Helper.getString(params, "description", true, "description");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			insert(fi, model);

			List<TablePageFilter> filter = new ArrayList<TablePageFilter>();
			filter.add(new TablePageFilter(C.id, C.columnTypeNumber, C.operationEqual, String.valueOf(model.id), null));
			TablePage p = new TablePage();
			String s = p.getRows(this, fi, 1, 1, this.getColumns(fi), null, filter, null, null, null);
			
			conn.commit();
			
			return s;
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	private String update(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		
		BanPhoneModel model = new BanPhoneModel();
		model.id = Helper.getInt(params, "id", true, "id");
		model.phone_no = Helper.getString(params, "phone_no", true, "phone_no");
		model.description = Helper.getString(params, "description", true, "description");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			update(fi, model);

			List<TablePageFilter> filter = new ArrayList<TablePageFilter>();
			filter.add(new TablePageFilter("id", "NUMBER", "=", String.valueOf(model.id), null));
			TablePage p = new TablePage();
			String s = p.getRows(this, fi, 1, 1, this.getColumns(fi), null, filter, null, null, null);
			
			conn.commit();
			
			return s;
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	private String delete(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		BanPhoneModel model = new BanPhoneModel();
		model.id = Helper.getInt(params, "id", true, "id");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			delete(fi, model);
			conn.commit();
			return "1";
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}

	public void insert(FunctionItem fi, BanPhoneModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("INSERT INTO `ban_phone_list` (`phone_no`, `description`) VALUES (?,?)");
		PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM `ban_phone_list` WHERE `phone_no` = ?");
		ResultSet r;
		try {
			pstmt.setString(1, model.phone_no);
			pstmt.setString(2, model.description);
			pstmt.executeUpdate();
			
			pstmt2.setString(1, model.phone_no);
			r = pstmt2.executeQuery();
			
			try {
				if (r.next()) {
					model.id = r.getInt("id");
				}
			}
			finally {
				r.close();
			}		
			
		}
		finally {
			pstmt.close();
			pstmt2.close();
		}
	}

	public void update(FunctionItem fi, BanPhoneModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("UPDATE `ban_phone_list` SET `phone_no` = ?, `description` = ? WHERE id = ?");
		try {
				pstmt.setString(1, model.phone_no);
				pstmt.setString(2, model.description);
				pstmt.setLong(3, model.id);
				pstmt.executeUpdate();
		}
		finally {
			pstmt.close();
		}
	}
	
	public void delete(FunctionItem fi, BanPhoneModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("DELETE FROM ban_phone_list where id = ?");
		try {
			pstmt.setLong(1, model.id);
			pstmt.executeUpdate();
		}
		finally {
			pstmt.close();
		}
	}
	
	public BanPhoneModel getDataForUpdate(FunctionItem fi, ITablePage page, int id) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("SELECT `id`, `phone_no`, `description` FROM " + page.getTableName() + " WHERE `id` = ?");
		ResultSet r;
		try {
			pstmt.setLong(1, id);
			r = pstmt.executeQuery();
			try {
				if (r.next()) {
					BanPhoneModel m = new BanPhoneModel();
					m.id = id;
					m.phone_no = r.getString("phone_no");
					m.description = r.getString("description");
					return m;
				}
				else
					throw new Exception("id [" + id + "] not exist");
			}
			finally {
				r.close();
			}
		}
		finally {
			pstmt.close();
		}
	}

}
