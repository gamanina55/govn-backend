package com.indogo.relay.config;

import java.sql.Timestamp;

public class BanPhoneModel {
	public long id;
	public String phone_no;
	public String description;
	public Timestamp updated;
}
