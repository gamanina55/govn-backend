package com.indogo.relay.config;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.indogo.relay.config.DocModel;
import com.lionpig.webui.database.ResultSetWrapperStringify;
import com.lionpig.webui.http.IFunction;
import com.lionpig.webui.http.func.TablePage;
import com.lionpig.webui.http.struct.FunctionItem;
import com.lionpig.webui.http.tablepage.ITablePage;
import com.lionpig.webui.http.tablepage.TablePageColumn;
import com.lionpig.webui.http.tablepage.TablePageFilter;
import com.lionpig.webui.http.tablepage.TablePageRowAttribute;
import com.lionpig.webui.http.tablepage.TablePageSort;
import com.lionpig.webui.http.util.C;
import com.lionpig.webui.http.util.Helper;

import java.sql.Connection;

public class DocConfiguration implements IFunction, ITablePage {

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp,
			FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		String action = Helper.getString(params, C.action, true);
		
		if (action.equals(C.insert))
			return this.insert(fi);
		else if (action.equals(C.update))
			return this.update(fi);
		else if (action.equals(C.delete))
			return this.delete(fi);
		else if (action.equals(C.getDataForUpdate)) {
			DocModel m = this.getDataForUpdate(fi, this, Helper.getInt(params, "id", true));

			StringBuilder sb = new StringBuilder();
			sb.append(Helper.replaceNull(m.name)).append(C.char_31)
			  .append(Helper.replaceNull(m.content));
			
			return sb.toString();
		} else
			throw new Exception(String.format(C.unknown_action, action));
	}

	@Override
	public String getTableOwner() {
		return null;
	}

	@Override
	public String getTableName() {
		return "doc_list";
	}

	@Override
	public List<TablePageColumn> getColumns(FunctionItem fi) {
		List<TablePageColumn> cols = new ArrayList<TablePageColumn>();
		cols.add(new TablePageColumn(C.id, C.columnTypeNumber, C.columnDirectionDefault, true, false, C.id, true, false));
		cols.add(new TablePageColumn(C.name, C.columnTypeString, C.columnDirectionDefault, true, false, C.name));
		cols.add(new TablePageColumn(C.content, C.columnTypeString, C.columnDirectionDefault, true, false, C.content));
		return cols;
	}

	@Override
	public void populateRowData(FunctionItem fi,
			Hashtable<String, TablePageColumn> cols, ResultSetWrapperStringify r,
			boolean isHtml, TablePageRowAttribute rowAttr) throws Exception {
		cols.get(C.id).setValue(r.getInt(C.id));
		cols.get(C.name).setValue(r.getString(C.name));
		cols.get(C.content).setValue(r.getString(C.content));
	}

	@Override
	public boolean populateRowDataBefore(FunctionItem fi,
			List<TablePageColumn> column, List<TablePageSort> sort,
			List<TablePageFilter> filter) throws Exception {
		return false;
	}

	@Override
	public void populateRowDataAfter(FunctionItem fi) throws Exception {
	}
	
	private String insert(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		
		DocModel model = new DocModel();
		model.name = Helper.getString(params, "name", true, "name");
		model.content = Helper.getString(params, "content", true, "content");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			insert(fi, model);

			List<TablePageFilter> filter = new ArrayList<TablePageFilter>();
			filter.add(new TablePageFilter(C.id, C.columnTypeNumber, C.operationEqual, String.valueOf(model.id), null));
			TablePage p = new TablePage();
			String s = p.getRows(this, fi, 1, 1, this.getColumns(fi), null, filter, null, null, null);
			
			conn.commit();
			
			return s;
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	private String update(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		
		DocModel model = new DocModel();
		model.id = Helper.getInt(params, "id", true, "id");
		model.name = Helper.getString(params, "name", true, "name");
		model.content = Helper.getString(params, "content", true, "content");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			update(fi, model);

			List<TablePageFilter> filter = new ArrayList<TablePageFilter>();
			filter.add(new TablePageFilter("id", "NUMBER", "=", String.valueOf(model.id), null));
			TablePage p = new TablePage();
			String s = p.getRows(this, fi, 1, 1, this.getColumns(fi), null, filter, null, null, null);
			
			conn.commit();
			
			return s;
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	private String delete(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		DocModel model = new DocModel();
		model.id = Helper.getInt(params, "id", true, "id");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			delete(fi, model);
			conn.commit();
			return "1";
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}

	public void insert(FunctionItem fi, DocModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("INSERT INTO `doc_list` (`name`, `content`) VALUES (?,?)");
		PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM `doc_list` WHERE `name` = ?");
		ResultSet r;
		try {
			pstmt.setString(1, model.name);
			pstmt.setString(2, model.content);
			pstmt.executeUpdate();
			
			pstmt2.setString(1, model.name);
			r = pstmt2.executeQuery();
			
			try {
				if (r.next()) {
					model.id = r.getInt("id");
				}
			}
			finally {
				r.close();
			}		
			
		}
		finally {
			pstmt.close();
			pstmt2.close();
		}
	}

	public void update(FunctionItem fi, DocModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("UPDATE `doc_list` SET `name` = ?, `content` = ? WHERE id = ?");
		try {
				pstmt.setString(1, model.name);
				pstmt.setString(2, model.content);
				pstmt.setLong(3, model.id);
				pstmt.executeUpdate();
		}
		finally {
			pstmt.close();
		}
	}
	
	public void delete(FunctionItem fi, DocModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("DELETE FROM `doc_list` WHERE `id` = ?");
		try {
			pstmt.setLong(1, model.id);
			pstmt.executeUpdate();
		}
		finally {
			pstmt.close();
		}
	}
	
	public DocModel getDataForUpdate(FunctionItem fi, ITablePage page, int id) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("SELECT `id`, `name`, `content` FROM " + page.getTableName() + " WHERE `id` = ?");
		ResultSet r;
		try {
			pstmt.setLong(1, id);
			r = pstmt.executeQuery();
			try {
				if (r.next()) {
					DocModel m = new DocModel();
					m.id = id;
					m.name = r.getString("name");
					m.content = r.getString("content");
					return m;
				}
				else
					throw new Exception("id [" + id + "] not exist");
			}
			finally {
				r.close();
			}
		}
		finally {
			pstmt.close();
		}
	}

}
