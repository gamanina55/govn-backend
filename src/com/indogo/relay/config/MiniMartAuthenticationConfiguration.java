package com.indogo.relay.config;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.indogo.MiniMart;
import com.indogo.relay.config.MiniMartAuthenticationModel;
import com.lionpig.webui.database.ResultSetWrapperStringify;
import com.lionpig.webui.http.IFunction;
import com.lionpig.webui.http.func.TablePage;
import com.lionpig.webui.http.struct.FunctionItem;
import com.lionpig.webui.http.tablepage.ITablePage;
import com.lionpig.webui.http.tablepage.TablePageColumn;
import com.lionpig.webui.http.tablepage.TablePageFilter;
import com.lionpig.webui.http.tablepage.TablePageRowAttribute;
import com.lionpig.webui.http.tablepage.TablePageSort;
import com.lionpig.webui.http.util.C;
import com.lionpig.webui.http.util.Helper;

import java.sql.Connection;

public class MiniMartAuthenticationConfiguration implements IFunction, ITablePage {

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp,
			FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		String action = Helper.getString(params, C.action, true);
		
		if (action.equals(C.insert))
			return this.insert(fi);
		else if (action.equals(C.update))
			return this.update(fi);
		else if (action.equals(C.delete))
			return this.delete(fi);
		else if (action.equals(C.getDataForUpdate)) {
			MiniMartAuthenticationModel m = this.getDataForUpdate(fi, this, Helper.getInt(params, "id", true));

			StringBuilder sb = new StringBuilder();
			sb.append(m.mini_mart_id).append(C.char_31)
			  .append(Helper.replaceNull(m.account)).append(C.char_31)
			  .append(Helper.replaceNull(m.password)).append(C.char_31)
			  .append(Helper.replaceNull(m.code));
			
			return sb.toString();
		} else if (action.equals(C.init)) {
			return this.getMiniMartList(fi);
		} else
			throw new Exception(String.format(C.unknown_action, action));
	}

	@Override
	public String getTableOwner() {
		return null;
	}

	@Override
	public String getTableName() {
		return "mini_mart_authentication";
	}

	@Override
	public List<TablePageColumn> getColumns(FunctionItem fi) {
		
		List<TablePageColumn> cols = new ArrayList<TablePageColumn>();
		cols.add(new TablePageColumn(C.id, C.columnTypeNumber, C.columnDirectionDefault, true, false, C.id, true, false));
		cols.add(new TablePageColumn(C.mini_mart_id, C.columnTypeNumber, C.columnDirectionDefault, true, false, C.mini_mart_id, true, false));
		cols.add(new TablePageColumn(C.account, C.columnTypeString, C.columnDirectionDefault, true, false, C.account));
		cols.add(new TablePageColumn(C.password, C.columnTypeString, C.columnDirectionDefault, true, false, C.password));
		cols.add(new TablePageColumn(C.code, C.columnTypeString, C.columnDirectionDefault, true, false, C.code, true, false));
		return cols;
	}

	@Override
	public void populateRowData(FunctionItem fi,
			Hashtable<String, TablePageColumn> cols, ResultSetWrapperStringify r,
			boolean isHtml, TablePageRowAttribute rowAttr) throws Exception {
		
		Integer mini_mart_id = Integer.parseInt(r.getInt(C.mini_mart_id));
		String mini_mart_name = MiniMart.get(mini_mart_id).getDisplay();
		
		cols.get(C.id).setValue(r.getInt(C.id));
		cols.get(C.mini_mart_id).setValue(mini_mart_name);
		cols.get(C.account).setValue(r.getString(C.account));
		cols.get(C.password).setValue(r.getString(C.password));
		cols.get(C.code).setValue(r.getString(C.code));
	}

	@Override
	public boolean populateRowDataBefore(FunctionItem fi,
			List<TablePageColumn> column, List<TablePageSort> sort,
			List<TablePageFilter> filter) throws Exception {
		return false;
	}

	@Override
	public void populateRowDataAfter(FunctionItem fi) throws Exception {
	}
	
	private String insert(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		
		MiniMartAuthenticationModel model = new MiniMartAuthenticationModel();
		model.mini_mart_id = Helper.getInt(params, "mini_mart_id", true, "mini_mart_id");
		model.account = Helper.getString(params, "account", true, "account");
		model.password = Helper.getString(params, "password", true, "password");
		model.code = Helper.getString(params, "code", false, "code");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			insert(fi, model);

			List<TablePageFilter> filter = new ArrayList<TablePageFilter>();
			filter.add(new TablePageFilter(C.id, C.columnTypeNumber, C.operationEqual, String.valueOf(model.id), null));
			TablePage p = new TablePage();
			String s = p.getRows(this, fi, 1, 1, this.getColumns(fi), null, filter, null, null, null);
			
			conn.commit();
			
			return s;
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	private String update(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		
		MiniMartAuthenticationModel model = new MiniMartAuthenticationModel();
		model.id = Helper.getInt(params, "id", true, "id");
		model.mini_mart_id = Helper.getInt(params, "mini_mart_id", true, "mini_mart_id");
		model.account = Helper.getString(params, "account", true, "account");
		model.password = Helper.getString(params, "password", true, "password");
		model.code = Helper.getString(params, "code", false, "code");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			update(fi, model);

			List<TablePageFilter> filter = new ArrayList<TablePageFilter>();
			TablePage p = new TablePage();
			String s = p.getRows(this, fi, 1, 1, this.getColumns(fi), null, filter, null, null, null);
			
			conn.commit();
			
			return s;
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	private String delete(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		MiniMartAuthenticationModel model = new MiniMartAuthenticationModel();
		model.id = Helper.getInt(params, "id", true, "id");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			delete(fi, model);
			conn.commit();
			return "1";
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}

	public void insert(FunctionItem fi, MiniMartAuthenticationModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("INSERT INTO `mini_mart_authentication` (`mini_mart_id`, `account`, `password`, `code`) VALUES (?,?,?,?)");
		PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM `mini_mart_authentication` WHERE `mini_mart_id` = ?");
		ResultSet r;
		try {
			pstmt.setInt(1, model.mini_mart_id);
			pstmt.setString(2, model.account);
			pstmt.setString(3, model.password);
			pstmt.setString(4, model.code);
			pstmt.executeUpdate();
			
			pstmt2.setInt(1, model.mini_mart_id);
			r = pstmt2.executeQuery();
			
			try {
				if (r.next()) {
					model.id = r.getInt("id");
				}
			}
			finally {
				r.close();
			}
		}
		finally {
			pstmt.close();
			pstmt2.close();
		}
	}

	public void update(FunctionItem fi, MiniMartAuthenticationModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("UPDATE `mini_mart_authentication` SET `mini_mart_id` = ?, `account` = ?, `password` = ?, `code` = ? WHERE id = ?");
		try {
			pstmt.setInt(1, model.mini_mart_id);
			pstmt.setString(2, model.account);
			pstmt.setString(3, model.password);
			pstmt.setString(4, model.code);
			pstmt.setInt(5, model.id);
			pstmt.executeUpdate();
		}
		finally {
			pstmt.close();
		}
	}
	
	public void delete(FunctionItem fi, MiniMartAuthenticationModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("DELETE FROM mini_mart_authentication where id = ?");
		try {
			pstmt.setInt(1, model.id);
			pstmt.executeUpdate();
		}
		finally {
			pstmt.close();
		}
	}
	
	public MiniMartAuthenticationModel getDataForUpdate(FunctionItem fi, ITablePage page, int id) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("SELECT `id`, `mini_mart_id`, `account`, `password`, `code` FROM " + page.getTableName() + " WHERE `id` = ?");
		ResultSet r;
		try {
			pstmt.setInt(1, id);
			r = pstmt.executeQuery();
			try {
				if (r.next()) {
					MiniMartAuthenticationModel m = new MiniMartAuthenticationModel();
					m.id = id;
					m.mini_mart_id = r.getInt("mini_mart_id");
					m.account = r.getString("account");
					m.password = r.getString("password");
					m.code = r.getString("code");
					return m;
				}
				else
					throw new Exception("id [" + id + "] not exist");
			}
			finally {
				r.close();
			}
		}
		finally {
			pstmt.close();
		}
	}
	
	private String getMiniMartList(FunctionItem fi) throws Exception {
		Statement stmt = fi.getConnection().getConnection().createStatement();
		try {
			StringBuilder sb = new StringBuilder();
			try (ResultSetWrapperStringify r = new ResultSetWrapperStringify(stmt.executeQuery("SELECT `mini_mart_id`, `mini_mart_name` FROM `mini_mart` ORDER BY `mini_mart_id` ASC"))) {
				while (r.next()) {
					Integer mini_mart_id = Integer.parseInt(r.getInt(1)); 
					String mini_mart_name = MiniMart.get(mini_mart_id).getDisplay();
					
					if(mini_mart_id == 1) {
						sb.append("<option value=\"").append(mini_mart_id).append("\" selected>").append(mini_mart_name).append("</option>");	
					} else {
						sb.append("<option value=\"").append(mini_mart_id).append("\">").append(mini_mart_name).append("</option>");
					}
				}
			}
			return sb.toString();
		} finally {
			stmt.close();
		}
	}

}
