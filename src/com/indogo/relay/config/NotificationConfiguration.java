package com.indogo.relay.config;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;

import com.google.gson.Gson;
import com.indogo.model.member.MemberModel;
import com.indogo.relay.config.NotificationModel;
import com.indogo.util.Image;
import com.lionpig.webui.database.ResultSetWrapperStringify;
import com.lionpig.webui.http.IFunction;
import com.lionpig.webui.http.func.TablePage;
import com.lionpig.webui.http.struct.FunctionItem;
import com.lionpig.webui.http.tablepage.ITablePage;
import com.lionpig.webui.http.tablepage.TablePageColumn;
import com.lionpig.webui.http.tablepage.TablePageFilter;
import com.lionpig.webui.http.tablepage.TablePageRowAttribute;
import com.lionpig.webui.http.tablepage.TablePageSort;
import com.lionpig.webui.http.util.C;
import com.lionpig.webui.http.util.Helper;

import java.io.InputStream;
import java.sql.Connection;

public class NotificationConfiguration implements IFunction, ITablePage {
	
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp,
			FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		String action = Helper.getString(params, C.action, true);
		
		if (action.equals(C.insert)) {
			return this.insert(fi);
		} else if (action.equals(C.update)) {
			return this.update(fi);
		} else if (action.equals(C.delete)) {
			return this.delete(fi);
		} else if(action.contentEquals(C.photo_upload)) {
			Hashtable<String, FileItem> uploadFiles = fi.getUploadedFiles();
			if (uploadFiles.size() > 0) {
				FileItem photo = uploadFiles.get(C.photo_upload);
				if (photo != null && photo.getName().length() > 0) {
					String photo_basename = Helper.getString(params, C.photo_basename, false);
					photo_basename = saveImage(fi, photo.getInputStream(), fi.getConnection().getGlobalConfig(C.notification_photo, C.image_base_directory), photo_basename);

					String baseUrl = fi.getConnection().getGlobalConfig(C.notification_photo, C.image_base_url);
					StringBuilder sb = new StringBuilder();
					sb.append(photo_basename).append(C.char_31)
					.append(baseUrl).append("/").append(photo_basename).append(".png");
					return sb.toString();
				} else
					throw new Exception("input [photo_upload] cannot be emtpy");
			} else
				throw new Exception("input [photo_upload] cannot be emtpy");
		} else if (action.equals(C.getDataForUpdate)) {
			int id = Helper.getInt(params, "id", true);
			return getDataForUpdate(fi, id);
		} else if (action.equals(C.findMember)) {
			Select2Model searchModel = getMembers(fi);
			String json = new Gson().toJson(searchModel);
	        return json;
		} else {
			throw new Exception(String.format(C.unknown_action, action));
		}
	}

	@Override
	public String getTableOwner() {
		return null;
	}

	@Override
	public String getTableName() {
		return "notification";
	}

	@Override
	public List<TablePageColumn> getColumns(FunctionItem fi) {
		List<TablePageColumn> cols = new ArrayList<TablePageColumn>();
		cols.add(new TablePageColumn(C.id, C.columnTypeNumber, C.columnDirectionDefault, true, false, C.id, true, false));
		cols.add(new TablePageColumn(C.name, C.columnTypeString, C.columnDirectionDefault, true, false, C.name));
		cols.add(new TablePageColumn(C.title, C.columnTypeString, C.columnDirectionDefault, true, false, C.title));
		cols.add(new TablePageColumn(C.content, C.columnTypeString, C.columnDirectionDefault, true, false, C.content));
		cols.add(new TablePageColumn(C.photo, C.columnTypeString, C.columnDirectionDefault, true, false, C.photo));
		cols.add(new TablePageColumn(C.link, C.columnTypeString, C.columnDirectionDefault, true, false, C.link));
		cols.add(new TablePageColumn(C.type, C.columnTypeString, C.columnDirectionDefault, true, false, C.type));
		cols.add(new TablePageColumn(C.status, C.columnTypeNumber, C.columnDirectionDefault, true, false, C.status));
		cols.add(new TablePageColumn(C.need_send_message, C.columnTypeNumber, C.columnDirectionDefault, true, false, "是否發送通知"));
		cols.add(new TablePageColumn(C.exec_option, C.columnTypeString, C.columnDirectionDefault, true, false, C.exec_option));
		cols.add(new TablePageColumn(C.exec_date, C.columnTypeString, C.columnDirectionDefault, true, false, C.exec_date));
		cols.add(new TablePageColumn(C.exec_times, C.columnTypeString, C.columnDirectionDefault, true, false, C.exec_times));
		cols.add(new TablePageColumn(C.member_ids, C.columnTypeString, C.columnDirectionDefault, true, false, C.member_ids));
		return cols;
	}

	@Override
	public void populateRowData(FunctionItem fi,
			Hashtable<String, TablePageColumn> cols, ResultSetWrapperStringify r,
			boolean isHtml, TablePageRowAttribute rowAttr) throws Exception {
		String photo = null;
		if(!Helper.isNullOrEmpty(r.getString(C.photo))) {
			photo = String.format("<img src='%s' />", formatPhotoUrl(fi, r.getString(C.photo)));
		}
				
		String status = Integer.parseInt(r.getInt(C.status)) == 1 ? "啟用" : "停用";
		String need_send_message = Integer.parseInt(r.getInt(C.need_send_message)) == 1 ? "會發送" : "不發送";
		String member_ids = r.getString(C.member_ids);
		
		if(member_ids != null && member_ids.isEmpty() == false) {
			List<MemberModel> members = getMembers(fi, member_ids);
			String separator = "";
			StringBuilder sb = new StringBuilder();
			for(MemberModel memberModel: members) {
				sb.append(separator).append(memberModel.arc_no);
			    separator = ",";
			}
			
			member_ids = sb.toString();			
		}
		
		cols.get(C.id).setValue(r.getInt(C.id));
		cols.get(C.name).setValue(r.getString(C.name));
		cols.get(C.title).setValue(r.getString(C.title));
		cols.get(C.content).setValue(r.getString(C.content));
		cols.get(C.photo).setValue(photo);
		cols.get(C.link).setValue(r.getString(C.link));
		cols.get(C.type).setValue(r.getString(C.type));
		cols.get(C.status).setValue(status);
		cols.get(C.need_send_message).setValue(need_send_message);
		cols.get(C.exec_option).setValue(r.getString(C.exec_option));
		cols.get(C.exec_date).setValue(r.getString(C.exec_date));
		cols.get(C.exec_times).setValue(r.getString(C.exec_times));
		cols.get(C.member_ids).setValue(member_ids);
	}

	@Override
	public boolean populateRowDataBefore(FunctionItem fi,
			List<TablePageColumn> column, List<TablePageSort> sort,
			List<TablePageFilter> filter) throws Exception {
		return false;
	}

	@Override
	public void populateRowDataAfter(FunctionItem fi) throws Exception {
	}
	
	public static String formatPhotoUrl(FunctionItem fi, String photo_basename) throws Exception {
		if(photo_basename.length() == 0) {
			return "";
		}
		
		String photoUrl = fi.getConnection().getGlobalConfig(C.notification_photo, C.image_base_url);
		StringBuilder sb = new StringBuilder();
		sb.append(photoUrl).append("/").append(photo_basename).append(".png");
		return sb.toString();
	}
	
	private String insert(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		
		NotificationModel model = new NotificationModel();
		model.name = Helper.getString(params, C.name, true, C.name);
		model.title = Helper.getString(params, C.title, true, C.title);
		model.content = Helper.getString(params, C.content, true, C.content);
		model.photo = Helper.getString(params, C.photo_basename, false, C.photo);
		model.link = Helper.getString(params, C.link, false, C.link);
		model.type = Helper.getString(params, C.type, true, C.type);
		model.status = Helper.getInt(params, C.status, false, C.status);
		model.need_send_message = Helper.getInt(params, C.need_send_message, false, C.need_send_message);
		model.exec_option = Helper.getString(params, C.exec_option, true, C.exec_option);
		model.exec_date = Helper.getString(params, C.exec_date, true, C.exec_date);
		model.exec_times = Helper.getString(params, C.exec_times, false, C.exec_times);
		model.member_ids = Helper.getString(params, C.member_ids, false, C.member_ids);
		
		Connection conn = fi.getConnection().getConnection();
		try {
			insert(fi, model);

			List<TablePageFilter> filter = new ArrayList<TablePageFilter>();
			filter.add(new TablePageFilter(C.id, C.columnTypeNumber, C.operationEqual, String.valueOf(model.id), null));
			TablePage p = new TablePage();
			String s = p.getRows(this, fi, 1, 1, this.getColumns(fi), null, filter, null, null, null);
			
			conn.commit();
			return s;
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	private String update(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();

		NotificationModel model = new NotificationModel();
		model.id = Helper.getInt(params, C.id, true, C.id);
		model.title = Helper.getString(params, C.title, true, C.title);
		model.name = Helper.getString(params, C.name, true, C.name);
		model.content = Helper.getString(params, C.content, true, C.content);
		model.photo = Helper.getString(params, C.photo_basename, false, C.photo);
		model.link = Helper.getString(params, C.link, false, C.link);
		model.type = Helper.getString(params, C.type, true, C.type);
		model.status = Helper.getInt(params, C.status, false, C.status);
		model.need_send_message = Helper.getInt(params, C.need_send_message, false, C.need_send_message);
		model.exec_option = Helper.getString(params, C.exec_option, true, C.exec_option);
		model.exec_date = Helper.getString(params, C.exec_date, true, C.exec_date);
		model.exec_times = Helper.getString(params, C.exec_times, false, C.exec_times);
		model.member_ids = Helper.getString(params, C.member_ids, false, C.member_ids);
		
		Connection conn = fi.getConnection().getConnection();
		try {
			update(fi, model);

			List<TablePageFilter> filter = new ArrayList<TablePageFilter>();
			filter.add(new TablePageFilter("id", "NUMBER", "=", String.valueOf(model.id), null));
			TablePage p = new TablePage();
			String s = p.getRows(this, fi, 1, 1, this.getColumns(fi), null, filter, null, null, null);
			
			conn.commit();
			return s;
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	private String delete(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		NotificationModel model = new NotificationModel();
		model.id = Helper.getInt(params, "id", true, "id");
		
		Connection conn = fi.getConnection().getConnection();
		try {
			delete(fi, model);
			conn.commit();
			return "1";
		}
		catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}

	public void insert(FunctionItem fi, NotificationModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("INSERT INTO `notification` (`name`, `title`, `content`, `photo`, `link`, `type`, `status`, `need_send_message`, `exec_option`, `exec_date`, `exec_times`, `member_ids`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM `notification` WHERE `name` = ? AND `content` = ?");
		ResultSet r;
		try {
			pstmt.setString(1, model.name);
			pstmt.setString(2, model.title);
			pstmt.setString(3, model.content);
			pstmt.setString(4, model.photo);
			pstmt.setString(5, model.link);
			pstmt.setString(6, model.type);
			pstmt.setInt(7, model.status);
			pstmt.setInt(8, model.need_send_message);
			pstmt.setString(9, model.exec_option);
			pstmt.setString(10, model.exec_date);
			pstmt.setString(11, model.exec_times);
			pstmt.setString(12, model.member_ids);
			pstmt.executeUpdate();
			
			pstmt2.setString(1, model.name);
			pstmt2.setString(2, model.content);
			r = pstmt2.executeQuery();
			
			try {
				if (r.next()) {
					model.id = r.getInt("id");
				}
			}
			finally {
				r.close();
			}		
			
		}
		finally {
			pstmt.close();
			pstmt2.close();
		}
	}

	public void update(FunctionItem fi, NotificationModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("UPDATE `notification` SET `name` = ?, `title` = ?, `content` = ?, `photo` = ?, `link` = ?, `type` = ?, `status` = ?, `need_send_message` = ?, `exec_option` = ?, `exec_date` = ?, `exec_times` = ?, `member_ids` = ? WHERE id = ?");
		try {
				pstmt.setString(1, model.name);
				pstmt.setString(2, model.title);
				pstmt.setString(3, model.content);
				pstmt.setString(4, model.photo);
				pstmt.setString(5, model.link);
				pstmt.setString(6, model.type);
				pstmt.setInt(7, model.status);
				pstmt.setInt(8, model.need_send_message);
				pstmt.setString(9, model.exec_option);
				pstmt.setString(10, model.exec_date);
				pstmt.setString(11, model.exec_times);
				pstmt.setString(12, model.member_ids);
				pstmt.setLong(13, model.id);
				pstmt.executeUpdate();
		}
		finally {
			pstmt.close();
		}
	}
	
	public void delete(FunctionItem fi, NotificationModel model) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("DELETE FROM `notification` WHERE `id` = ?");
		try {
			pstmt.setLong(1, model.id);
			pstmt.executeUpdate();
		}
		finally {
			pstmt.close();
		}
	}
	
	public String getDataForUpdate(FunctionItem fi, int id) throws Exception {
		StringBuilder sb = new StringBuilder();
		NotificationModel model = this.getData(fi, id);

		sb.append(model.id).append(C.char_31)
		  .append(Helper.replaceNull(model.name)).append(C.char_31)
		  .append(Helper.replaceNull(model.title)).append(C.char_31)
		  .append(Helper.replaceNull(model.content)).append(C.char_31)
		  .append(Helper.replaceNull(model.photo)).append(C.char_31)
		  .append(Helper.replaceNull(model.link)).append(C.char_31)
		  .append(Helper.replaceNull(model.type)).append(C.char_31)
		  .append(model.status).append(C.char_31)
		  .append(model.need_send_message).append(C.char_31)
		  .append(Helper.replaceNull(model.exec_option)).append(C.char_31)
		  .append(Helper.replaceNull(model.exec_date)).append(C.char_31)
		  .append(Helper.replaceNull(model.exec_times)).append(C.char_31)
		  .append(Helper.replaceNull(model.member_ids)).append(C.char_31)
		  .append(formatPhotoUrl(fi, Helper.replaceNull(model.photo)));
		
		return sb.toString();
	}
	
	public NotificationModel getData(FunctionItem fi, long id) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM `notification` WHERE `id` = ?");
		ResultSet r;
		try {
			pstmt.setLong(1, id);
			r = pstmt.executeQuery();
			try {
				if (r.next()) {
					NotificationModel model = new NotificationModel();
					model.id = r.getInt(1);
					model.name = r.getString(2);
					model.title = r.getString(3);
					model.content = r.getString(4);
					model.photo = r.getString(5);
					model.link = r.getString(6);
					model.type = r.getString(7);
					model.status = r.getInt(8);
					model.need_send_message = r.getInt(9);
					model.exec_option = r.getString(10);
					model.exec_date = r.getString(11);
					model.exec_times = r.getString(12);
					model.member_ids = r.getString(13);
					return model;
				}
				else
					throw new Exception("id [" + id + "] not exist");
			}
			finally {
				r.close();
			}
		}
		finally {
			pstmt.close();
		}
	}
	
	private List<MemberModel> getMembers(FunctionItem fi, String member_ids) throws Exception {
		List<MemberModel> list = new ArrayList<MemberModel>();
		String query = String.format("select `member_id` AS `id`, `arc_no` AS `text` from `member` where `is_wait_confirm` in(3,4) AND `member_id` IN(%s) ORDER BY `member_id` ASC", member_ids);
		PreparedStatement pstmt = fi.getConnection().getConnection().prepareStatement(query);
		
		try {
			try (ResultSet r = pstmt.executeQuery()) {
				while (r.next()) {
					MemberModel model = new MemberModel();
					model.member_id = r.getLong(1);
					model.arc_no = r.getString(2);
					list.add(model);
				}
			}
		} finally {
			pstmt.close();
		}
		
		return list;
	}
	
	public static Select2Model getMembers(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		String arc_no = Helper.getString(params, C.arc_no, false, C.arc_no);
		String member_ids = Helper.getString(params, C.member_ids, false, C.member_ids);
		String arc_nos = Helper.getString(params, "arc_nos", false, "arc_nos");
		
		int page = Helper.getInt(params, "page", true, "page");
		int offset = Helper.getInt(params, "offset", true, "offset");
		
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		int totalCount = 0;
		
		String query = String.format("select `member_id` AS `id`, `arc_no` AS `text` from `member` where `is_wait_confirm` IN (3,4) AND `arc_no` LIKE '%s' ORDER BY `member_id` ASC LIMIT %s, %s", arc_no, (page - 1) * offset, offset);		
		String query2 = String.format("select COUNT(*) AS `count` from `member` where `is_wait_confirm` IN (3,4) = 0 AND `arc_no` LIKE '%s'", arc_no);
		
		if(member_ids != null && member_ids.isEmpty() == false) {
			query = String.format("select `member_id` AS `id`, `arc_no` AS `text` from `member` where `is_wait_confirm` IN (3,4) AND `member_id` IN(%s) ORDER BY `member_id` ASC", member_ids);
			query2 = String.format("select COUNT(*) AS `count` from `member` where `is_wait_confirm` IN (3,4) AND `member_id` IN(%s)", member_ids);
		} else if(arc_nos != null && arc_nos.isEmpty() == false) {
			query = String.format("select `member_id` AS `id`, `arc_no` AS `text` from `member` where `is_wait_confirm` IN (3,4) AND `arc_no` IN(%s) ORDER BY `member_id` ASC", arc_nos);
			query2 = String.format("select COUNT(*) AS `count` from `member` where `is_wait_confirm` IN (3,4) AND `arc_no` IN(%s)", arc_nos);
		}
		
		PreparedStatement pstmt = fi.getConnection().getConnection().prepareStatement(query);
		PreparedStatement pstmt2 = fi.getConnection().getConnection().prepareStatement(query2);
		try {
			try (ResultSet r = pstmt.executeQuery()) {
				while (r.next()) {
					HashMap<String, String> row = new HashMap<String, String>();
					row.put("id", String.valueOf(r.getLong(1)));
					row.put("text", r.getString(2));
					list.add(row);
				}
			}
			
			try (ResultSet r = pstmt2.executeQuery()) {
				if(r.next()) {
					totalCount = r.getInt(1);
				}
			}
		} finally {
			pstmt.close();
			pstmt2.close();
		}
		
		Select2Model searchModel = new Select2Model(list, totalCount, page, offset);
		return searchModel;
	}
	
 	private String saveImage(FunctionItem fi, InputStream imageSource, String remoteUri, String deleteOldFilename) throws Exception {
		String baseName = UUID.randomUUID().toString().replaceAll("-", "");
		String filename = String.format("%s.png", baseName);
		String deleteFilePath = null;
		if(!Helper.isNullOrEmpty(deleteOldFilename)) {
			deleteFilePath = String.format("%s/%s.png", remoteUri, deleteOldFilename);
		}
		
		int width = Integer.valueOf(fi.getConnection().getGlobalConfig(C.notification_photo, "photo_resize_width"));			
		String format = fi.getConnection().getGlobalConfig(C.notification_photo, "photo_format");
		return Image.saveImage(fi, imageSource, baseName, remoteUri, deleteFilePath, filename, width, format);
	}
 	
}
