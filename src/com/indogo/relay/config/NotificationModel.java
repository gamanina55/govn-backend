package com.indogo.relay.config;

public class NotificationModel {
	public long id;
	public String name;
	public String title;
	public String content;
	public String photo;
	public String link;
	public String type;
	public int status;
	public int need_send_message;
	public String exec_option;
	public String exec_date;
	public String exec_times;
	public String member_ids;
	public Double kurs_value;
}
