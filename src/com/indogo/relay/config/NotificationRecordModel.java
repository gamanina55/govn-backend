package com.indogo.relay.config;

import java.sql.Timestamp;

public class NotificationRecordModel {
	public long id;
	public long notification_id;
	public long member_id;
	public String title;
	public String content;
	public String photo;
	public String link;
	public String type;
	public int isReadable;
	public Timestamp create_time;
}
