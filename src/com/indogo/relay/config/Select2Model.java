package com.indogo.relay.config;

import java.util.HashMap;
import java.util.List;

public class Select2Model {
	private List<HashMap<String, String>> results;
	private HashMap<String, Boolean> pagination;
	private int totalCount = 0;
	private int page = 1;
	private int offset = 50;
	
	public Select2Model(List<HashMap<String, String>> results, int totalCount, int page, int offset) {
		this.results = results;
		this.totalCount = totalCount;
		this.page = page;
		this.offset = offset;
		this.pagination = new HashMap<String, Boolean>();
		this.pagination.put("more", false);
//		this.pagination.put("more", results.size() == this.offset);
	}
}
