package com.indogo.relay.member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.lionpig.webui.database.ResultSetWrapperStringify;
import com.lionpig.webui.database.RoleNameListModel;
import com.lionpig.webui.http.IFunction;
import com.lionpig.webui.http.struct.FunctionItem;
import com.lionpig.webui.http.tablepage.ITablePage;
import com.lionpig.webui.http.tablepage.TablePageColumn;
import com.lionpig.webui.http.tablepage.TablePageFilter;
import com.lionpig.webui.http.tablepage.TablePageRowAttribute;
import com.lionpig.webui.http.tablepage.TablePageSort;
import com.lionpig.webui.http.util.C;
import com.lionpig.webui.http.util.DateFormat;
import com.lionpig.webui.http.util.Helper;

/**
 * <pre>
 * 程式名稱	匯率設定
 * 程式功能	查看及更新匯率
 * 修改歷程:
 * [202001015]
 * 1.kurs_history新增kurs_type欄位
 * 2.更新匯率時同時寫入VND及USD
 * </pre>
 * @author Arvin
 *
 */
public class KursConfiguration implements IFunction, ITablePage {

	@Override
	public String getTableOwner() {
		return null;
	}

	@Override
	public String getTableName() {
		return C.kurs_history;
	}

	@Override
	public List<TablePageColumn> getColumns(FunctionItem fi) {
		List<TablePageColumn> cols = new ArrayList<>();
		cols.add(new TablePageColumn(C.lm_time, C.columnTypeDateTime, C.columnDirectionDesc, true, false, "Last Modified Time"));
		cols.add(new TablePageColumn(C.lm_user, C.columnTypeString, C.columnDirectionDefault, true, false, "User"));
		cols.add(new TablePageColumn(C.kurs_value, C.columnTypeNumber, C.columnDirectionDefault, true, false, "Kurs Value"));
		cols.add(new TablePageColumn(C.kurs_type, C.columnTypeNumber, C.columnDirectionDefault, true, false, "Kurs Type"));
		return cols;
	}

	@Override
	public void populateRowData(FunctionItem fi, Hashtable<String, TablePageColumn> cols, ResultSetWrapperStringify r,
			boolean isHtml, TablePageRowAttribute rowAttr) throws Exception {
		cols.get(C.lm_time).setValue(r.getTimestamp(C.lm_time));
		cols.get(C.lm_user).setValue(r.getString(C.lm_user));
		cols.get(C.kurs_value).setValue(r.getDouble(C.kurs_value));
		cols.get(C.kurs_type).setValue(r.getString(C.kurs_type));
	}

	@Override
	public boolean populateRowDataBefore(FunctionItem fi, List<TablePageColumn> column, List<TablePageSort> sort,
			List<TablePageFilter> filter) throws Exception {
		return false;
	}

	@Override
	public void populateRowDataAfter(FunctionItem fi) throws Exception {
	}

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp, FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		String action = Helper.getString(params, C.action, true);
		
		if (action.equals(C.update)) {
			double newVNDKursValue = Helper.getDouble(params, "vnd_kurs", true);
			double newUSDKursValue = Helper.getDouble(params, "usd_kurs", true);
			int send = Helper.getInt(params, "send", true);
	    	
			Connection conn = fi.getConnection().getConnection();
			//[20201015] 更新匯率時同時寫入VND及USD
			PreparedStatement pstmt = conn.prepareStatement("INSERT INTO `kurs_history` (`lm_time`, `lm_user`, `kurs_value`, `kurs_type`) VALUES (?,?,?,?), (?,?,?,?)");
			
			try {
				String s = fi.getConnection().getGlobalConfig(C.member, C.kurs_value);
				if (s != null) {
					double oldKursValue = Double.parseDouble(s);
					if (newVNDKursValue == oldKursValue) {// TODO
						throw new Exception(C.nothing_to_update);
					}
				}
				
				Timestamp lm_time = fi.getConnection().getCurrentTime();
				
				String stringVNDKursValue = formatKurs(String.valueOf(newVNDKursValue));// TODO
				String stringUSDKursValue = formatKurs(String.valueOf(newUSDKursValue));// TODO

				fi.getConnection().setGlobalConfig(C.member, C.kurs_value, stringVNDKursValue, true);// TODO
				fi.getConnection().setGlobalConfig(C.member, "kurs_usd_value", stringUSDKursValue, true);// TODO
				fi.getConnection().setGlobalConfig(C.member, C.kurs_lm_time, DateFormat.getInstance().format(lm_time), true);
				int i = 1;
				String userName = fi.getSessionInfo().getUserName();
				pstmt.setTimestamp(i++, lm_time);
				pstmt.setString(i++, userName);
				pstmt.setDouble(i++, newVNDKursValue);
				pstmt.setString(i++, "VND");
				pstmt.setTimestamp(i++, lm_time);
				pstmt.setString(i++, userName);
				pstmt.setDouble(i++, newUSDKursValue);
				pstmt.setString(i++, "USD");
				pstmt.executeUpdate();
			
				conn.commit();
				
				if(send == 1) {
					execNotificationRecordHistory(fi, newVNDKursValue);// TODO
				}
			} catch (Exception e) {
				conn.rollback();
				throw new Exception(C.update_fail);
			} finally {
				pstmt.close();
//				conn.close();
			}
			
			return StringUtils.EMPTY;
		} else if (action.equals(C.init)) {
			StringBuilder sb = new StringBuilder();
			List<RoleNameListModel> roles = fi.getConnection().adminUserGetRoles(fi.getSessionInfo().getUserName());
			sb.append(roles.size());
			for (RoleNameListModel role : roles) {
				sb.append(C.char_31).append(role.ROLE_ID);
			}
			return sb.toString();
		} else {
			throw new Exception(String.format(C.unknown_action, action));
		}
	}
	
	/**
	 * 若匯率小數點第一位為0則無條件捨去
	 * @param stringKursValue
	 * @return
	 */
	private String formatKurs(String stringKursValue) {
		char[] charsKursValue = stringKursValue.toCharArray();
		if (charsKursValue[charsKursValue.length - 2] == '.' && charsKursValue[charsKursValue.length - 1] == '0') {
			stringKursValue = new String(charsKursValue, 0, charsKursValue.length - 2);
		}
		return stringKursValue;
	}

	private void execNotificationRecordHistory(FunctionItem fi, double newKursValue) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		
		List<Long> member_ids = new ArrayList<Long>();
		String query1 = "SELECT `member_id` FROM `member_device_tokens`";
		PreparedStatement stmt1 = conn.prepareStatement(query1);
		ResultSet r = stmt1.executeQuery();
		try {		
        	while(r.next()) {                        		     		
        		long member_id = r.getLong(1);
        		if(member_ids.indexOf(member_id) == -1) {
        			member_ids.add(member_id);
        		}  		
        	}
    	} finally {
    		r.close();
    		stmt1.close();
    	}
		
		int notification_id = 0;
		String content = null;
    	String query2 = "SELECT `id`, `content` FROM `notification` WHERE `name` = ?";
    	PreparedStatement stmt2 = conn.prepareStatement(query2);
    	stmt2.setString(1, "kurs");
		r = stmt2.executeQuery();
    	try {
        	if(r.next()) {                        		     		
        		notification_id = r.getInt(1);
        		content = String.format(r.getString(2), newKursValue);
        	}
    	} finally {
    		r.close();
    		stmt2.close();
    	}
    	
		NotificationRecordHistory.sendNotificationNotRecordInTable(fi, StringUtils.join(member_ids, ","), notification_id, content);
	}
	
}
