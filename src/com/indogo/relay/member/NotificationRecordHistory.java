package com.indogo.relay.member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.indogo.TransferStatus;
import com.indogo.model.member.MemberModel;
import com.indogo.model.member.MoneyTransferModel;
import com.indogo.relay.config.NotificationConfiguration;
import com.indogo.relay.config.NotificationModel;
import com.indogo.relay.config.Select2Model;
import com.indogo.util.FirebaseMessage;
import com.lionpig.webui.database.ResultSetWrapperStringify;
import com.lionpig.webui.http.IFunction;
import com.lionpig.webui.http.struct.FunctionItem;
import com.lionpig.webui.http.tablepage.ITablePage;
import com.lionpig.webui.http.tablepage.TablePageColumn;
import com.lionpig.webui.http.tablepage.TablePageFilter;
import com.lionpig.webui.http.tablepage.TablePageRowAttribute;
import com.lionpig.webui.http.tablepage.TablePageSort;
import com.lionpig.webui.http.util.C;
import com.lionpig.webui.http.util.Helper;

public class NotificationRecordHistory implements IFunction, ITablePage {

	@Override
	public String getTableOwner() {
		return null;
	}

	@Override
	public String getTableName() {
		return C.notification_record;
	}

	@Override
	public List<TablePageColumn> getColumns(FunctionItem fi) {
		List<TablePageColumn> cols = new ArrayList<TablePageColumn>();
		cols.add(new TablePageColumn(C.id, C.columnTypeNumber, C.columnDirectionDefault, true, false, "id"));
		cols.add(new TablePageColumn(C.notification_id, C.columnTypeString, C.columnDirectionDefault, true, false, "通知名稱"));
		cols.add(new TablePageColumn(C.title, C.columnTypeString, C.columnDirectionDefault, true, false, "通知標題"));
		cols.add(new TablePageColumn(C.content, C.columnTypeString, C.columnDirectionDefault, false, false, "通知內容"));
		cols.add(new TablePageColumn(C.photo, C.columnTypeString, C.columnDirectionDefault, false, false, "通知圖片"));
		cols.add(new TablePageColumn(C.link, C.columnTypeString, C.columnDirectionDefault, false, false, "通知連結"));
		cols.add(new TablePageColumn(C.type, C.columnTypeString, C.columnDirectionDefault, true, false, "通知類型"));
		cols.add(new TablePageColumn(C.member_id, C.columnTypeString, C.columnDirectionDefault, true, false, "通知帳號"));
		cols.add(new TablePageColumn(C.isReadable, C.columnTypeString, C.columnDirectionDefault, true, false, "讀取狀態"));
		cols.add(new TablePageColumn(C.create_time, C.columnTypeDateTime, C.columnDirectionDefault, true, false, "建立時間"));
		return cols;
	}

	@Override
	public void populateRowData(FunctionItem fi, Hashtable<String, TablePageColumn> cols, ResultSetWrapperStringify r,
			boolean isHtml, TablePageRowAttribute rowAttr) throws Exception {
		String isReadable = Integer.parseInt(r.getInt(C.isReadable)) == 1 ? "已讀取" : "未讀取";
		MemberModel member = getMember(fi, r.getLong(C.member_id));
		NotificationModel notification = getNotification(fi, r.getLong(C.notification_id));
		String photo = null;
		if(!Helper.isNullOrEmpty(r.getString(C.photo))) {
			photo = String.format("<img src='%s' />", NotificationConfiguration.formatPhotoUrl(fi, r.getString(C.photo)));
		}
		
		String memberName = member.member_name;
		if(!Helper.isNullOrEmpty(member.arc_no)) {
			memberName = String.format("%s(%s)", memberName, member.arc_no);
		}
				
		cols.get(C.id).setValue(r.getLong(C.id));
		cols.get(C.notification_id).setValue(notification.name);
		cols.get(C.title).setValue(r.getString(C.title));
		cols.get(C.content).setValue(r.getString(C.content));
		cols.get(C.photo).setValue(photo);
		cols.get(C.link).setValue(r.getString(C.link));
		cols.get(C.type).setValue(r.getString(C.type));
		cols.get(C.member_id).setValue(memberName);
		cols.get(C.isReadable).setValue(isReadable);
		cols.get(C.create_time).setValue(r.getTimestamp(C.create_time));
	}

	@Override
	public boolean populateRowDataBefore(FunctionItem fi, List<TablePageColumn> column, List<TablePageSort> sort,
			List<TablePageFilter> filter) throws Exception {
		sort.add(new TablePageSort(C.create_time, C.columnDirectionDesc));
		sort.add(new TablePageSort(C.id, C.columnDirectionDesc));
		return false;
	}

	@Override
	public void populateRowDataAfter(FunctionItem fi) throws Exception {
	}

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp,
			FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		String action = Helper.getString(params, C.action, true);
		
		if (action.equals(C.findMember)) {
			Select2Model searchModel = NotificationConfiguration.getMembers(fi);
			String json = new Gson().toJson(searchModel);
	        return json;
		} else if (action.equals(C.findNotification)) {
			Select2Model searchModel = getNotifications(fi);
			String json = new Gson().toJson(searchModel);
	        return json;
		} else if (action.equals("sendNotification")) {
			String member_ids = Helper.getString(params, "member_ids", true);
			int notification_id = Helper.getInt(params, "notification_id", true);
			String content = Helper.getString(params, "content", true);
			return sendNotification(fi, member_ids, notification_id, content);
		} else if (action.equals("money_transfer")) {
			String txn_id = Helper.getString(params, "txn_id", true);
			return execMoneyTransfer(fi, txn_id);
		} else {
			throw new Exception(String.format(C.unknown_action, action));
		}
	}
	
	private MemberModel getMember(FunctionItem fi, String member_id) throws Exception {
		MemberModel model = null;
		String query = String.format("select `member_id`, `member_name`, `arc_no` from `member` where `is_wait_confirm` IN (3,4) AND `member_id` = %s", member_id);
		PreparedStatement pstmt = fi.getConnection().getConnection().prepareStatement(query);
		
		try {
			try (ResultSet r = pstmt.executeQuery()) {
				if (r.next()) {
					model = new MemberModel();
					model.member_id = r.getLong(1);
					model.member_name = r.getString(2);
					model.arc_no = r.getString(3);
				}
			}
		} finally {
			pstmt.close();
		}
		
		return model;
	}
	
	private NotificationModel getNotification(FunctionItem fi, String notification_id) throws Exception {
		NotificationModel model = new NotificationModel();
		if(!Helper.isNullOrEmpty(notification_id)) {		
			String query = String.format("select * from `notification` where `id` = %s", notification_id);
			PreparedStatement pstmt = fi.getConnection().getConnection().prepareStatement(query);
			try {
				try (ResultSet r = pstmt.executeQuery()) {
					if (r.next()) {
						model.id = r.getLong(1);
						model.name = r.getString(2);
						model.title = r.getString(3);
						model.content = r.getString(4);
						model.photo = r.getString(5);
						model.link = r.getString(6);
						model.type = r.getString(7);
						model.status = r.getInt(8);
						model.exec_option = r.getString(9);
						model.exec_date = r.getString(10);
						model.exec_times = r.getString(11);
						model.member_ids = r.getString(12);
					}
				}
			} finally {
				pstmt.close();
			}
		}
		
		return model;
	}
		
	public static Select2Model getNotifications(FunctionItem fi) throws Exception {
		Hashtable<String, String> params = fi.getRequestParameters();
		String name = Helper.getString(params, C.name, false, C.name);
		
		int page = Helper.getInt(params, "page", true, "page");
		int offset = Helper.getInt(params, "offset", true, "offset");
		
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		int totalCount = 0;
		
		String query = String.format("select `id`, `name` AS `text` from `notification` where `name` LIKE '%s' ORDER BY `id` ASC LIMIT %s, %s", name, (page - 1) * offset, offset);		
		String query2 = String.format("select COUNT(*) AS `count` from `notification` where `name` LIKE '%s'", name);
		
		PreparedStatement pstmt = fi.getConnection().getConnection().prepareStatement(query);
		PreparedStatement pstmt2 = fi.getConnection().getConnection().prepareStatement(query2);
		try {
			try (ResultSet r = pstmt.executeQuery()) {
				while (r.next()) {
					HashMap<String, String> row = new HashMap<String, String>();
					row.put("id", String.valueOf(r.getLong(1)));
					row.put("text", r.getString(2));
					list.add(row);
				}
			}
			
			try (ResultSet r = pstmt2.executeQuery()) {
				if(r.next()) {
					totalCount = r.getInt(1);
				}
			}
		} finally {
			pstmt.close();
			pstmt2.close();
		}
		
		Select2Model searchModel = new Select2Model(list, totalCount, page, offset);
		return searchModel;
	}
	
	public static String execMoneyTransfer(FunctionItem fi, String txn_id) throws Exception {
		Connection conn = fi.getConnection().getConnection();

		String status = "false";
		MoneyTransferModel moneyTransfer = null;
		NotificationModel notification = null;
		MemberModel member = null;
		String notification_name = null;
		int transfer_status_id = 0;
				
		//String query1 = "SELECT a.`member_id`, a.`transfer_status_id`, a.`payment_info`, b.`member_name` FROM `money_transfer` AS a LEFT JOIN `member` AS b ON b.`member_id` = a.`member_id` WHERE a.`txn_id` = ?";
		/*
		 * SELECT transfer.`member_id`, transfer.`transfer_status_id`, transfer.`payment_info`, recipient.`recipient_name`
		 * 		FROM `money_transfer` AS transfer 
		 *  LEFT JOIN `member_recipient` AS recipient
		 *      ON recipient.`recipient_id` = transfer.`recipient_id`
		 *  WHERE transfer.`txn_id` = ?
		 */
		String query1 = "SELECT transfer.`member_id`, transfer.`transfer_status_id`, transfer.`payment_info`, recipient.`recipient_name` FROM `money_transfer` AS transfer LEFT JOIN `member_recipient` AS recipient ON recipient.`recipient_id` = transfer.`recipient_id` WHERE transfer.`txn_id` = ?";
		PreparedStatement stmt1 = conn.prepareStatement(query1);
		
		String query2 = "SELECT * FROM `notification` WHERE `status` = 1 AND `name` = ?";
		PreparedStatement stmt2 = conn.prepareStatement(query2);
						
		try {
			stmt1.setString(1, txn_id);
			ResultSet r = stmt1.executeQuery();
			try {
				if(r.next()) {
					moneyTransfer = new MoneyTransferModel();
					moneyTransfer.member_id = r.getLong(1);
					moneyTransfer.transfer_status_id = TransferStatus.get(r.getInt(2));
					moneyTransfer.payment_info = r.getString(3);
					
					member = new MemberModel();
					member.member_id = r.getLong(1);
					member.member_name = r.getString(4);
					
					transfer_status_id = r.getInt(2);
				}
			} finally {
				r.close();
			}
		} finally {
			stmt1.close();
		}

		if(moneyTransfer == null) {
			throw new Exception(String.format("txn_id: %s not found", txn_id));
		}
		
		if(member == null) {
			throw new Exception(String.format("txn_id: %s, member_id not found", txn_id));
		}
			
		if(transfer_status_id == 6) {
			status = "OK";
			return status;
		}

		switch(transfer_status_id) {
			case 1:
				notification_name = "money_transfer_pending";
				break;
			case 2:
				notification_name = "money_transfer_paid";
				break;
			case 3:
				notification_name = "money_transfer_process";
				break;
			case 4:
				notification_name = "money_transfer_transfered";
				break;
			case 5:
				notification_name = "money_transfer_failed";
				break;
		}
					
		try {	
			stmt2.setString(1, notification_name);
			ResultSet r = stmt2.executeQuery();
			try {
				if(r.next()) {
					notification = new NotificationModel();
					notification.id = r.getInt(1);
					notification.content = r.getString(4);
	        		
	        		if(transfer_status_id == 4) {
	        			notification.content = String.format(notification.content, moneyTransfer.payment_info, member.member_name);
	        		} else {
	        			notification.content = String.format(notification.content, moneyTransfer.payment_info);
	        		}
				}
			} finally {
				r.close();
			}						
		} finally {
			stmt2.close();
		}
		
		if(notification == null) {
//			status = "OK";
//			return status;
            throw new Exception(String.format("notification: %s, not found", notification_name));
		}
		
		return sendNotification(fi, String.valueOf(moneyTransfer.member_id), (int) notification.id, notification.content);
	}
	
 	public static String sendNotification(FunctionItem fi, String member_ids, int notification_id, String content) throws Exception {
		String status = "failed";
		List<String> deviceTokens = new ArrayList<String>();
		String baseURL = fi.getConnection().getGlobalConfig(C.notification_photo, C.image_base_url);
		NotificationModel notification = null;
		
		Connection conn = fi.getConnection().getConnection();
		String query1 = "SELECT * FROM `notification` WHERE `status` = 1 AND `id` = ?";
		String query2 = String.format("INSERT INTO `notification_record`(`notification_id`, `title`, `content`, `photo`, `link`, `type`, `member_id`, `create_time`) SELECT ?, ?, ?, ?, ?, ?, `member_id`, NOW() FROM `member` WHERE `is_wait_confirm` IN (3,4) AND `member_id` IN (%s)", member_ids);
		String query3 = String.format("SELECT b.`token` FROM `member` AS a LEFT JOIN `member_device_tokens` AS b ON b.`member_id` = a.`member_id` WHERE a.`is_wait_confirm` IN (3,4) AND b.`token` IS NOT NULL AND a.`member_id` IN (%s)", member_ids);
		String query4 = String.format("SELECT `kurs_value` FROM `kurs_history` ORDER BY `lm_time` DESC LIMIT 1");
		
		PreparedStatement stmt1 = conn.prepareStatement(query1);
		PreparedStatement stmt2 = conn.prepareStatement(query2);
    	PreparedStatement stmt3 = conn.prepareStatement(query3);
		PreparedStatement stmt4 = conn.prepareStatement(query4);
    	
		try {
			stmt1.setInt(1, notification_id);
			ResultSet r = stmt1.executeQuery();
			try {
				if(r.next()) {
					notification = new NotificationModel();
					notification.id = r.getInt(1);
					notification.name = r.getString(2);
					notification.title = r.getString(3);
					notification.content = content;
					notification.photo = r.getString(5);
					notification.link = r.getString(6);
					notification.type = r.getString(7);
					notification.status = r.getInt(8);
					notification.need_send_message = r.getInt(9);
	        		notification.exec_option = r.getString(10);
	        		notification.exec_date = r.getString(11);
	        		notification.exec_times = r.getString(12);
	        		notification.member_ids = r.getString(13);
				}
			} finally {
				r.close();
			}
			
			if(notification == null) {
//				status = "OK";
//				return status;
                throw new Exception("Notification not found");
			}
			
			try {
				stmt2.setLong(1, notification.id);
		        stmt2.setString(2, notification.title);
		        stmt2.setString(3, notification.content);
		        stmt2.setString(4, notification.photo);
		        stmt2.setString(5, notification.link);
		        stmt2.setString(6, notification.type);
		    	stmt2.executeUpdate();
		    	conn.commit();
			} finally {
				r.close();
			}
			
			r = stmt3.executeQuery();
			try {
				while(r.next()) {
					deviceTokens.add(r.getString(1));
				}
			} finally {
				r.close();
			}
			
			if(notification.type.equals("匯率變更")) {
				r = stmt4.executeQuery();
				try {
					if(r.next()) {
						notification.kurs_value = r.getDouble(1);
					}
				} finally {
					r.close();
				}
			}
						
			if(notification.need_send_message == 1 && deviceTokens.size() > 0) {
        		if(baseURL != null && baseURL.isEmpty() == false && notification.photo != null && notification.photo.isEmpty() == false) {
        			notification.photo = NotificationConfiguration.formatPhotoUrl(fi, notification.photo);
    			}
								
				FirebaseMessage.sendMessage(fi.getServletContext(), deviceTokens, notification);
			}
			
			status = "OK";
		} finally {
			stmt1.close();
			stmt2.close();
			stmt3.close();
			stmt4.close();
		}
		
		return status;
	}
 	
 	public static String sendNotificationNotRecordInTable(FunctionItem fi, String member_ids, int notification_id, String content) throws Exception {
		String status = "failed";
		List<String> deviceTokens = new ArrayList<String>();
		String baseURL = fi.getConnection().getGlobalConfig(C.notification_photo, C.image_base_url);
		NotificationModel notification = null;
		
		Connection conn = fi.getConnection().getConnection();
		String query1 = "SELECT * FROM `notification` WHERE `status` = 1 AND `id` = ?";
		String query3 = String.format("SELECT b.`token` FROM `member` AS a LEFT JOIN `member_device_tokens` AS b ON b.`member_id` = a.`member_id` WHERE a.`is_wait_confirm` IN (3,4) AND b.`token` IS NOT NULL AND a.`member_id` IN (%s)", member_ids);
		String query4 = String.format("SELECT `kurs_value` FROM `kurs_history` ORDER BY `lm_time` DESC LIMIT 1");
		
		PreparedStatement stmt1 = conn.prepareStatement(query1);
    	PreparedStatement stmt3 = conn.prepareStatement(query3);
		PreparedStatement stmt4 = conn.prepareStatement(query4);
    	
		try {
			stmt1.setInt(1, notification_id);
			ResultSet r = stmt1.executeQuery();
			try {
				if(r.next()) {
					notification = new NotificationModel();
					notification.id = r.getInt(1);
					notification.name = r.getString(2);
					notification.title = r.getString(3);
					notification.content = content;
					notification.photo = r.getString(5);
					notification.link = r.getString(6);
					notification.type = r.getString(7);
					notification.status = r.getInt(8);
					notification.need_send_message = r.getInt(9);
	        		notification.exec_option = r.getString(10);
	        		notification.exec_date = r.getString(11);
	        		notification.exec_times = r.getString(12);
	        		notification.member_ids = r.getString(13);
				}
			} finally {
				r.close();
			}
			
			if(notification == null) {
//				status = "OK";
//				return status;
                throw new Exception("Notification not found");
			}
			
			r = stmt3.executeQuery();
			try {
				while(r.next()) {
					deviceTokens.add(r.getString(1));
				}
			} finally {
				r.close();
			}
			
			if(notification.type.equals("匯率變更")) {
				r = stmt4.executeQuery();
				try {
					if(r.next()) {
						notification.kurs_value = r.getDouble(1);
					}
				} finally {
					r.close();
				}
			}
						
			if(notification.need_send_message == 1 && deviceTokens.size() > 0) {
        		if(baseURL != null && baseURL.isEmpty() == false && notification.photo != null && notification.photo.isEmpty() == false) {
        			notification.photo = NotificationConfiguration.formatPhotoUrl(fi, notification.photo);
    			}
								
				FirebaseMessage.sendMessage(fi.getServletContext(), deviceTokens, notification);
			}
			
			status = "OK";
		} finally {
			stmt1.close();
			stmt3.close();
			stmt4.close();
		}
		
		return status;
	}
}
