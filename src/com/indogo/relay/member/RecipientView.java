package com.indogo.relay.member;

import java.awt.image.BufferedImage;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.IdentityInfo;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;

import com.indogo.model.config.UserModel;
import com.indogo.model.member.BankCodeModel;
import com.indogo.relay.member.BankCodeConfiguration.OrderBy;
import com.lionpig.language.L;
import com.lionpig.sql.S;
import com.lionpig.webui.database.PreparedStatementWrapper;
import com.lionpig.webui.database.ResultSetWrapper;
import com.lionpig.webui.database.ResultSetWrapperStringify;
import com.lionpig.webui.http.AbstractFunction;
import com.lionpig.webui.http.struct.FunctionItem;
import com.lionpig.webui.http.tablepage.ITablePage;
import com.lionpig.webui.http.tablepage.TablePageColumn;
import com.lionpig.webui.http.tablepage.TablePageFilter;
import com.lionpig.webui.http.tablepage.TablePageRowAttribute;
import com.lionpig.webui.http.tablepage.TablePageSort;
import com.lionpig.webui.http.util.C;
import com.lionpig.webui.http.util.Helper;
import com.lionpig.webui.http.util.Stringify;
import com.indogo.relay.config.EmailConfiguration;

public class RecipientView extends AbstractFunction implements ITablePage {
	
	private String recipient_photo_url;

	@Override
	public String getTableOwner() {
		return null;
	}

	@Override
	public String getTableName() {
		return C.member_recipient_v;
	}

	@Override
	public List<TablePageColumn> getColumns(FunctionItem fi) {
		List<TablePageColumn> cols = new ArrayList<TablePageColumn>();
		cols.add(new TablePageColumn(C.recipient_name, C.columnTypeString, C.columnDirectionDefault, true, false, "Recipient Name"));
		cols.add(new TablePageColumn(C.recipient_name_2, C.columnTypeString, C.columnDirectionDefault, true, false, "Recipient Name 2"));
		cols.add(new TablePageColumn(C.bank_code, C.columnTypeString, C.columnDirectionDefault, true, false, "Bank Code"));
		cols.add(new TablePageColumn(C.bank_name, C.columnTypeString, C.columnDirectionDefault, true, false, "Bank Name"));
		cols.add(new TablePageColumn(C.swift_code, C.columnTypeString, C.columnDirectionDefault, true, false, "SWIFT"));
		cols.add(new TablePageColumn(C.bank_acc, C.columnTypeString, C.columnDirectionDefault, true, false, "Bank Account"));
		cols.add(new TablePageColumn(C.birthday, C.columnTypeDateTime, C.columnDirectionDefault, true, false, "Birthday"));
		cols.add(new TablePageColumn(C.id_image, C.columnTypeString, C.columnDirectionNone, false, true, "ID Picture"));
		cols.add(new TablePageColumn(C.lm_time, C.columnTypeDateTime, C.columnDirectionDefault, true, false, C.lm_time));
		cols.add(new TablePageColumn(C.lm_user, C.columnTypeString, C.columnDirectionDefault, true, false, C.lm_user));
		cols.add(new TablePageColumn(C.member_id, C.columnTypeNumber, C.columnDirectionDefault, true, false, C.member_id, true, true));
		cols.add(new TablePageColumn(C.recipient_id, C.columnTypeNumber, C.columnDirectionDefault, true, false, C.recipient_id, true, true));
		cols.add(new TablePageColumn(C.is_verified, C.columnTypeNumber, C.columnDirectionDefault, true, false, C.is_verified, true, true));
		cols.add(new TablePageColumn(C.is_hidden, C.columnTypeNumber, C.columnDirectionDefault, true, false, C.is_hidden, true, true));
		cols.add(new TablePageColumn(C.id_filename, C.columnTypeString, C.columnDirectionDefault, true, false, C.id_filename, true, true));
		return cols;
	}

	@Override
	public void populateRowData(FunctionItem fi, Hashtable<String, TablePageColumn> cols, ResultSetWrapperStringify r,
			boolean isHtml, TablePageRowAttribute rowAttr) throws Exception {
		long member_id = r.unwrap().getLong(C.member_id);
		int recipient_id = r.unwrap().getInt(C.recipient_id);
		String id_filename = r.getString(C.id_filename);
		String id_image = getRecipientPhotoUrl(recipient_photo_url, member_id, recipient_id, id_filename);
		
		cols.get(C.recipient_name).setValue(r.getString(C.recipient_name));
		cols.get(C.recipient_name_2).setValue(r.getString(C.recipient_name_2));
		cols.get(C.bank_code).setValue(r.getString(C.bank_code));
		cols.get(C.bank_name).setValue(r.getString(C.bank_name));
		cols.get(C.swift_code).setValue(r.getString(C.swift_code));
		cols.get(C.bank_acc).setValue(r.getString(C.bank_acc));
		cols.get(C.birthday).setValue(r.getDate(C.birthday));
		cols.get(C.id_image).setValue(id_image);
		cols.get(C.lm_time).setValue(r.getTimestamp(C.lm_time));
		cols.get(C.lm_user).setValue(r.getString(C.lm_user));
		cols.get(C.member_id).setValue(String.valueOf(member_id));
		cols.get(C.recipient_id).setValue(String.valueOf(recipient_id));
		cols.get(C.is_verified).setValue(r.getInt(C.is_verified));
		cols.get(C.is_hidden).setValue(r.getInt(C.is_hidden));
		cols.get(C.id_filename).setValue(id_filename);
	}

	@Override
	public boolean populateRowDataBefore(FunctionItem fi, List<TablePageColumn> column, List<TablePageSort> sort,
			List<TablePageFilter> filter) throws Exception {
		recipient_photo_url = fi.getConnection().getGlobalConfig(C.member, C.recipient_photo_url);
		return false;
	}

	@Override
	public void populateRowDataAfter(FunctionItem fi) throws Exception {
	}

	@Override
	protected String onExecute(HttpServletRequest req, HttpServletResponse resp, FunctionItem fi,
			Hashtable<String, String> params, String action, L l, S s) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String onInit(HttpServletRequest req, HttpServletResponse resp, FunctionItem fi,
			Hashtable<String, String> params, String action, L l, S s) throws Exception {
		StringBuilder sb = new StringBuilder(100);
		
		List<BankCodeModel> bankCodeList = new BankCodeConfiguration().getBankCodeList(fi, OrderBy.bank_code);
		sb.append(bankCodeList.size());
		for (BankCodeModel bankCodeModel : bankCodeList) {
			sb.append(C.char_31).append(bankCodeModel.bank_code)
			.append(C.char_31).append(bankCodeModel.bank_name);
		}
		
		bankCodeList = new BankCodeConfiguration().getBankCodeList(fi, OrderBy.bank_name);
		sb.append(C.char_31).append(bankCodeList.size());
		for (BankCodeModel bankCodeModel : bankCodeList) {
			sb.append(C.char_31).append(bankCodeModel.bank_code)
			.append(C.char_31).append(bankCodeModel.bank_name);
		}
		
		return sb.toString();
	}

	@Override
	protected String onInsert(HttpServletRequest req, HttpServletResponse resp, FunctionItem fi,
			Hashtable<String, String> params, String action, L l, S s) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String onUpdate(HttpServletRequest req, HttpServletResponse resp, FunctionItem fi,
			Hashtable<String, String> params, String action, L l, S s) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		
		String type_name = Helper.getString(params, C.type_name, true);
		if (type_name.equals(C.birthday)) {
			long member_id = Helper.getLong(params, C.member_id, true);
			int recipient_id = Helper.getInt(params, C.recipient_id, true);
			Timestamp birthday = Helper.getTimestamp(params, C.birthday, false);
			
			try {
				updateBirthday(fi, member_id, recipient_id, birthday);
				conn.commit();
				return Stringify.getDate(birthday);
			} catch (Exception e) {
				conn.rollback();
				throw e;
			}
		} else if (type_name.equals(C.id_image)) {
			Hashtable<String, FileItem> uploadFiles = fi.getUploadedFiles();
			if (uploadFiles.size() > 0) {
				FileItem photo = uploadFiles.get(C.id_image);
				if (photo != null && photo.getName().length() > 0) {
					long member_id = Helper.getLong(params, C.member_id, true);
					int recipient_id = Helper.getInt(params, C.recipient_id, true);
					
					try {
						String filename = updateIdImage(fi, member_id, recipient_id, photo);
						conn.commit();
						
						String recipient_photo_url = fi.getConnection().getGlobalConfig(C.member, C.recipient_photo_url);
						return recipient_photo_url + "/" + member_id + "/" + recipient_id + "/" + filename + ".png";
					} catch (Exception e) {
						conn.rollback();
						throw e;
					}
				}
			}
		} else if (type_name.equals(C.recipient_name_2)) {
			long member_id = Helper.getLong(params, C.member_id, true);
			int recipient_id = Helper.getInt(params, C.recipient_id, true);
			String recipient_name_2 = Helper.getString(params, C.recipient_name_2, false);
			
			try {
				updateRecipientName2(fi, member_id, recipient_id, recipient_name_2);
				conn.commit();
				return recipient_name_2;
			} catch (Exception e) {
				conn.rollback();
				throw e;
			}
		}
		return null;
	}

	@Override
	protected String onDelete(HttpServletRequest req, HttpServletResponse resp, FunctionItem fi,
			Hashtable<String, String> params, String action, L l, S s) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String onGet(HttpServletRequest req, HttpServletResponse resp, FunctionItem fi,
			Hashtable<String, String> params, String action, L l, S s) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void updateBirthday(FunctionItem fi, long member_id, int recipient_id, Timestamp birthday) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		
		try (PreparedStatementWrapper pstmt = new PreparedStatementWrapper(conn.prepareStatement("update member_recipient set birthday = ? where member_id = ? and recipient_id = ?"))) {
			pstmt.setTimestamp(1, birthday);
			pstmt.setLong(2, member_id);
			pstmt.setInt(3, recipient_id);
			pstmt.executeUpdate();
		}
	}
	
	public String updateIdImage(FunctionItem fi, long member_id, int recipient_id, FileItem photo) throws Exception {
		L l = fi.getLanguage();
		Connection conn = fi.getConnection().getConnection();
		
		String old_id_filename;
		PreparedStatementWrapper pstmt = new PreparedStatementWrapper(conn.prepareStatement("SELECT `id_filename` FROM `member_recipient` WHERE `member_id` = ? AND `recipient_id` = ? for update"));
		try {
			pstmt.setLong(1, member_id);
			pstmt.setInt(2, recipient_id);
			try (ResultSetWrapper r = pstmt.executeQueryWrapper()) {
				if (r.next()) {
					old_id_filename = r.getString(1);
				} else {
					throw new Exception(l.data_not_exist("member_id=" + member_id + ", recipient_id=" + recipient_id));
				}
			}
		} finally {
			pstmt.close();
		}

		String new_id_filename = UUID.randomUUID().toString().replaceAll("-", "");
		PreparedStatementWrapper pstmt1 = new PreparedStatementWrapper(conn.prepareStatement("UPDATE `member_recipient` set `id_filename` = ? WHERE `member_id` = ? AND `recipient_id` = ?"));
		try {
			pstmt1.setString(1, new_id_filename);
			pstmt1.setLong(2, member_id);
			pstmt1.setInt(3, recipient_id);
			pstmt1.executeUpdate();
		} finally {
			pstmt1.close();
		}
		
		BufferedImage bufferedImage = ImageIO.read(photo.getInputStream());
		File file = new File(fi.getTempFolder(), new_id_filename);
		if (!ImageIO.write(bufferedImage, "png", file)) {
			throw new Exception("cannot save photo");
		}
		
		String recipient_photo_directory = fi.getConnection().getGlobalConfig(C.member, C.recipient_photo_directory);
		
		StandardFileSystemManager manager = new StandardFileSystemManager();
		try {
			manager.init();
			
			String ssh_key_path = fi.getConnection().getGlobalConfig("item", "ssh_key_path");

			FileSystemOptions opts = new FileSystemOptions();
			SftpFileSystemConfigBuilder builder = SftpFileSystemConfigBuilder.getInstance();
			builder.setStrictHostKeyChecking(opts, "no");
			builder.setUserDirIsRoot(opts, true);
			builder.setTimeout(opts, 10000);
			
			if(ssh_key_path != null && ssh_key_path.length() > 0) {
				IdentityInfo identityInfo =  new IdentityInfo(new File(ssh_key_path));
				builder.setIdentityInfo(opts, identityInfo);
			}

			FileObject remoteBase = manager.resolveFile(recipient_photo_directory + "/" + member_id + "/" + recipient_id, opts);
			remoteBase.createFolder();
			
			if (old_id_filename != null) {
				FileObject remoteFile = manager.resolveFile(remoteBase, old_id_filename + ".png", opts);
				try {
					remoteFile.delete();
				} catch (Exception ignore) {}
			}
			
			FileObject localFile = manager.resolveFile(file.getAbsolutePath());
			FileObject newFile = manager.resolveFile(remoteBase, new_id_filename + ".png", opts);
			localFile.moveTo(newFile);
		} finally {
			manager.close();
		}

		ArrayList<String> emails = new ArrayList<>(); 
		PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM `user_list` WHERE `receiveIsBanMail` = true");	
		try (ResultSet r = pstmt2.executeQuery()) {
			if(r.next()) {
				UserModel row = new UserModel();
				row.user_row_id = r.getInt("user_row_id");
				row.user_name = r.getString("user_name");
				row.email_address = r.getString("email_address");
				row.password = r.getString("password");
				row.disabled = Helper.replaceNull(r.getString("disabled")).equals("Y");
				row.alias_id = r.getString("alias_id");
				row.color_id = r.getString("color_id");
				row.receiveIsBanMail = r.getInt("receiveIsBanMail");
				emails.add(row.email_address);
			}
		} finally {
			pstmt2.close();
		}
				
		ArrayList<String> transfers = new ArrayList<>();
		ArrayList<Long> txnIds = new ArrayList<>();
		ArrayList<String> paymentInfos = new ArrayList<>();
		PreparedStatement pstmt3 = conn.prepareStatement("SELECT `payment_info`, `txn_id`, `member_name`, `arc_no`, `bank_code`, `recipient_name`, `bank_acc`, `lm_time`, `total` from `money_transfer_v` where `member_id` = ? AND `recipient_id` = ? AND `is_ban` = true");
		pstmt3.setLong(1, member_id);
		pstmt3.setInt(2, recipient_id);
		try (ResultSet r = pstmt3.executeQuery()) {
			if (r.next()) {
				String payment_info = r.getString(1);
				Long txn_id = r.getLong(2);
				String member_name = r.getString(3);
				String arc_no = r.getString(4);				
				String bank_code = r.getString(5);
				String recipient_name = r.getString(6);
				String bank_acc = r.getString(7);
				String lm_time = Stringify.getTimestamp(r.getTimestamp(8));
				int total = r.getInt(9);
				
				transfers.add(
					String.format("<div>" + 
						"<p>Payment Info: %s</p>" + 
						"<p>Invoice單號： %s</p>" + 
						"<p>用戶名： %s</p>" + 
						"<p>ARC： %s</p>" + 
						"<p>Bank Code： %s</p>" + 
						"<p>Recipient Name： %s</p>" + 
						"<p>Bank Account： %s</p>" + 
						"<p>lm_time： %s</p>" + 
						"<p>Total (NTD)： %s</p>" + 
						"</div>", 
						payment_info, 
						txn_id, 
						member_name, 
						arc_no, 
						bank_code, 
						recipient_name, bank_acc, lm_time, total
					)
				);  
				
				txnIds.add(txn_id);
				paymentInfos.add(payment_info);
			}
		} finally {
			pstmt3.close();
		}
		
		if(transfers.size() == 0 || emails.size() == 0) {
			return new_id_filename;	
		}

		Timestamp newLmTime = fi.getConnection().getCurrentTime();
		Integer newIsBan = 0;
		int index = 0;
		for(Long txnId : txnIds) {
			PreparedStatement pstmt4 = conn.prepareStatement("SELECT `lm_time`, `is_ban` FROM `money_transfer` WHERE `txn_id` = ?");
			PreparedStatement pstmt5 = conn.prepareStatement("UPDATE `money_transfer` SET `is_ban` = ? WHERE `txn_id` = ?");
			PreparedStatement pstmt6 = conn.prepareStatement("INSERT INTO `money_transfer_is_ban` (`txn_id`, `lm_time`, `lm_user`, `old_is_ban`, `new_is_ban`) values (?,?,?,?,?)");
			
			Timestamp oldLmTime;
			Integer oldIsBan;
			String lmUser = fi.getSessionInfo().getUserName();
			try {
				pstmt4.setLong(1, txnId);
				try (ResultSet r = pstmt4.executeQuery()) {
					if (r.next()) {
						oldLmTime = r.getTimestamp(1);
						oldIsBan = r.getInt(2);
					} else {
						throw new Exception(String.format(C.data_not_exist, "txn_id = " + txnId));
					}
				}
				
				pstmt5.setInt(1, newIsBan);
				pstmt5.setLong(2, txnId);
				pstmt5.executeUpdate();
				
				pstmt6.setLong(1, txnId);
				pstmt6.setTimestamp(2, newLmTime);
				pstmt6.setString(3, lmUser);
				pstmt6.setInt(4, oldIsBan);
				pstmt6.setInt(5, newIsBan);
				pstmt6.executeUpdate();
			} catch (Exception e) {
				throw e;
			} finally {
				pstmt4.close();
				pstmt5.close();
				pstmt6.close();
			}
			
			String to = String.join(",", emails);
			String subject = String.format("訂單之帳號已更新照片-%s", paymentInfos.get(index));
			String htmlBody = String.join("", transfers);
			
			try {
				EmailConfiguration.sendMail(fi, to, subject, htmlBody);
			} catch (Exception e) {
				throw e;
			}
			
			index++;
	    }		
		
		return new_id_filename;	
	}
	
	public void updateRecipientName2(FunctionItem fi, long member_id, int recipient_id, String recipient_name_2) throws Exception {
		Connection conn = fi.getConnection().getConnection();
		
		try (PreparedStatementWrapper pstmt = new PreparedStatementWrapper(conn.prepareStatement("update member_recipient set recipient_name_2 = ? where member_id = ? and recipient_id = ?"))) {
			pstmt.setString(1, recipient_name_2);
			pstmt.setLong(2, member_id);
			pstmt.setInt(3, recipient_id);
			pstmt.executeUpdate();
		}
	}
	
	public static String getRecipientPhotoUrl(String recipient_photo_url, long member_id, int recipient_id, String id_filename) {
		if (Helper.isNullOrEmpty(id_filename)) {
			return C.emptyString;
		} else {
			return recipient_photo_url + "/" + member_id + "/" + recipient_id + "/" + id_filename + ".png";
		}
	}

}
