package com.indogo.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import com.google.api.client.util.Base64;
import com.lionpig.webui.http.struct.FunctionItem;

public class FareastSMS implements ISMS {
	
	@SuppressWarnings({ "unused", "finally" })
	private boolean exec_send(FunctionItem fi, String app_phone_no, String message) {
		boolean status = true;
		String ServerURL = "http://61.20.35.179:6500/mpsiweb/smssubmit";
		try {
			String sys_id = fi.getConnection().getGlobalConfig("fareastsms", "system_id");
			String src_address = fi.getConnection().getGlobalConfig("fareastsms", "src_address");
			String text = new String(Base64.encodeBase64(message.getBytes("UTF-8")));
			String XmlData ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
					"<SmsSubmitReq>" +
					"<SysId>" + sys_id + "</SysId>" +
					"<SrcAddress>" + src_address + "</SrcAddress>" +
					"<DestAddress>" + app_phone_no + "</DestAddress>" +
					"<SmsBody>" + text + "</SmsBody>" +
					"<DrFlag>true</DrFlag>" +
					"</SmsSubmitReq>";
			
			String encodeString = "xml=" + URLEncoder.encode(XmlData, "UTF-8");
			URL url = new URL(ServerURL);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			
			OutputStream outStream = conn.getOutputStream();
			outStream.write(encodeString.getBytes("UTF-8"));
			conn.connect();
			
			int responseCode = conn.getResponseCode();
			
			InputStream inStream = conn.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));
			String line = null;
			try {
				while((line = reader.readLine())!= null){
					System.out.println(line);
				}	
			} finally {
				reader.close();
			}
		} catch(Exception ignore) {
			status = false;
		} finally {
			return status;
		}
	}
	
	@SuppressWarnings("finally")
	public boolean send(FunctionItem fi, String app_phone_no) {
		boolean status = false;
		try {
			String message = fi.getConnection().getGlobalConfig("kotsms", "register_complete_message");
			status = this.exec_send(fi, app_phone_no, message);
		} catch(Exception ignore) {
			status = false;
		} finally {
			return status;
		}
	}

	public boolean send(FunctionItem fi, String app_phone_no, String message) {
		return this.exec_send(fi, app_phone_no, message);
	}
}
