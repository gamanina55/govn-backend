package com.indogo.util;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidConfig.Priority;
import com.google.firebase.messaging.ApnsConfig;
import com.google.firebase.messaging.Aps;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.indogo.relay.config.NotificationModel;

public class FirebaseMessage {

	private static void initApp(ServletContext context) throws Exception {
		String keyFilePath = context.getRealPath(context.getInitParameter("FileBaseKeyFile"));
		String firebaseURL = context.getInitParameter("FIREBASE_URL");

		FileInputStream serviceAccount = new FileInputStream(keyFilePath);
		FirebaseOptions options  = new FirebaseOptions.Builder()
		        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
		        .setDatabaseUrl(firebaseURL)
		        .build();

		try {
	        FirebaseApp.getInstance();
	    }
	    catch (IllegalStateException e) {
	    	FirebaseApp.initializeApp(options);
	    }		
	}
	
	/** 寄送眾多通知 **/
	private static void execSend(Notification notification, List<String> deviceTokens, NotificationModel notificationModel) throws Exception {
		int index = 0;
		int limit = 100;//每批最多送100則
		int tokenListSize = deviceTokens.size();
		
		List<List<String>> tokens = new ArrayList<List<String>>(tokenListSize % 100 == 0 ? tokenListSize / 100 : tokenListSize / 100 + 1);
		List<String> nowBatch = null;
		for(String deviceToken: deviceTokens) {			
			if(index++ % limit == 0) {
				//每滿100則通知就新建一個List
				int remainTokens = tokenListSize - index;//剩餘Token數
				int nowBatchInitSize = remainTokens < limit ? remainTokens : limit;//如果剩餘Token數量小於limit，只需要new剩餘Token的數量大小的List就好
				nowBatch = new ArrayList<String>(nowBatchInitSize);
				tokens.add(nowBatch);
			}
			nowBatch.add(deviceToken);
		}

		Map<String, String> data = new HashMap<String, String>();
		data.put("id", String.valueOf(notificationModel.id));
		if(notificationModel.link != null && notificationModel.link.isEmpty() == false) {
			data.put("link", notificationModel.link);
		}
		
		if(notificationModel.kurs_value != null && notificationModel.kurs_value > 0) {
			data.put("kurs_value", notificationModel.kurs_value.toString());
		}
		
		AndroidConfig androidConfig = AndroidConfig.builder().setPriority(Priority.HIGH).build();
		Aps aps = Aps.builder().setSound("default").build();
		ApnsConfig apnsConfig = ApnsConfig.builder().setAps(aps).build();
		for(List<String> entry: tokens) {
			MulticastMessage message = MulticastMessage.builder()
				.setAndroidConfig(androidConfig)
				.putAllData(data)
				.setNotification(notification)
			    .addAllTokens(entry)
			    .setApnsConfig(apnsConfig)
			    .build();
		
	    	BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(message);
			System.out.println(response.getSuccessCount() + " messages were sent successfully");
		}
	}
		
	@SuppressWarnings("deprecation")
	public static void sendMessage(ServletContext context, List<String> deviceTokens, NotificationModel notificationModel) throws Exception {		
		if(notificationModel.need_send_message == 0 || deviceTokens.size() == 0) {
			return;
		}
		
		initApp(context);
					
		Notification notification = new Notification(notificationModel.title, notificationModel.content);				
		if(notificationModel.photo != null && notificationModel.photo.isEmpty() == false) {
			notification = new Notification(notificationModel.title, notificationModel.content, notificationModel.photo);
		}
		
		execSend(notification, deviceTokens, notificationModel);	
	}
}
