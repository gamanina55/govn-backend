package com.indogo.util;

import com.lionpig.webui.http.struct.FunctionItem;

public interface ISMS {
	boolean send(FunctionItem fi, String app_phone_no);
	boolean send(FunctionItem fi, String app_phone_no, String message);
}
