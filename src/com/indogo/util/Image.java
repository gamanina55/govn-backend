package com.indogo.util;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.io.File;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.commons.vfs2.FileContent;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.IdentityInfo;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;
import org.apache.poi.util.IOUtils;

import com.lionpig.webui.http.struct.FunctionItem;
import com.lionpig.webui.http.util.Helper;

public class Image {
	private static FileSystemOptions getSFTP(FunctionItem fi) throws Exception {
		String ssh_key_path = fi.getConnection().getGlobalConfig("item", "ssh_key_path");
		FileSystemOptions opts = new FileSystemOptions();
		SftpFileSystemConfigBuilder builder = SftpFileSystemConfigBuilder.getInstance();
		builder.setStrictHostKeyChecking(opts, "no");
		builder.setUserDirIsRoot(opts, true);
		builder.setTimeout(opts, 10000);

		if(ssh_key_path != null && ssh_key_path.length() > 0) {
			IdentityInfo identityInfo =  new IdentityInfo(new File(ssh_key_path));
			builder.setIdentityInfo(opts, identityInfo);
		}		
		
		return opts;
	}
	
	private static BufferedImage resizeImage(BufferedImage img, String format, int resizeWidth) throws Exception {				
		int type = img.getType();
		int width = img.getWidth();
		int height = img.getHeight();
		
		if(format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpeg")) {
			type = BufferedImage.TYPE_INT_BGR;
		}
		
		if(width <= resizeWidth) {
			resizeWidth = width;
		}
		
		int resizeHeight = (int)((float) resizeWidth / width * height);
		
		BufferedImage resizeimg = new BufferedImage(resizeWidth, resizeHeight, type);
        Graphics2D g2d = resizeimg.createGraphics();
        g2d.drawImage(img, 0, 0, resizeWidth, resizeHeight, null);
        g2d.dispose();
        return resizeimg;
	}
	
	public static byte[] readImage(FunctionItem fi, String filepath) throws Exception {
		StandardFileSystemManager manager = new StandardFileSystemManager();
		try {
			manager.init();
						
			FileSystemOptions opts = getSFTP(fi);
			FileObject file = manager.resolveFile(filepath, opts);
			if (file.exists()) {
				FileContent content = file.getContent();
				InputStream stream = content.getInputStream();
				try {
					return IOUtils.toByteArray(stream);
				} finally {
					stream.close();
				}
			} else {
				return null;
			}
		} finally {
			manager.close();
		}
	}
	
	public static String saveImage(FunctionItem fi, InputStream imageSource, String baseName, String remoteUri, String deleteFilePath, String filename, int resizeWidth, String format) throws Exception {
		File file = new File(fi.getTempFolder(), baseName);
		BufferedImage img = ImageIO.read(imageSource);
		img = resizeImage(img, format, resizeWidth);
		if (!ImageIO.write(img, format, file)) {
			throw new Exception("cannot save photo");
		}
		
		StandardFileSystemManager manager = new StandardFileSystemManager();
		try {
			manager.init();

			FileSystemOptions opts = getSFTP(fi);
			FileObject localFile = manager.resolveFile(file.getAbsolutePath());					
			FileObject remoteBase = manager.resolveFile(remoteUri, opts);
			FileObject remoteFile = manager.resolveFile(remoteBase, filename, opts);
			localFile.moveTo(remoteFile);
			
			if (!Helper.isNullOrEmpty(deleteFilePath)) {
				remoteFile = manager.resolveFile(deleteFilePath, opts);
				try {
					remoteFile.delete();
				} catch (Exception ignore) {
					
				}
			}
			
			return baseName;
		} finally {
			manager.close();
		}
				
	}

	public static String saveImage(FunctionItem fi, InputStream imageSource, String baseName, String remoteUri, String deleteFilePath, String filename) throws Exception {
		File file = new File(fi.getTempFolder(), baseName);
		BufferedImage img = ImageIO.read(imageSource);
		if (!ImageIO.write(img, "JPEG", file)) {
			throw new Exception("cannot save photo");
		}
		
		StandardFileSystemManager manager = new StandardFileSystemManager();
		try {
			manager.init();

			FileSystemOptions opts = getSFTP(fi);
			FileObject localFile = manager.resolveFile(file.getAbsolutePath());					
			FileObject remoteBase = manager.resolveFile(remoteUri, opts);
			FileObject remoteFile = manager.resolveFile(remoteBase, filename, opts);
			localFile.moveTo(remoteFile);
			
			if (!Helper.isNullOrEmpty(deleteFilePath)) {
				remoteFile = manager.resolveFile(deleteFilePath, opts);
				try {
					remoteFile.delete();
				} catch (Exception ignore) {
					
				}
			}
			
			return baseName;
		} finally {
			manager.close();
		}
				
	}
	
	public static void moveImage(FunctionItem fi, String fromFilePath, String toBasePath, String toFilePath) throws Exception {
		StandardFileSystemManager manager = new StandardFileSystemManager();
		try {
			manager.init();
						
			FileSystemOptions opts = getSFTP(fi);
			FileObject fromFile = manager.resolveFile(fromFilePath, opts);
			FileObject toBase = manager.resolveFile(toBasePath, opts);
			toBase.createFolder();
			FileObject toFile = manager.resolveFile(toBase, toFilePath, opts);
			fromFile.moveTo(toFile);
		} finally {
			manager.close();
		}
	}
}
