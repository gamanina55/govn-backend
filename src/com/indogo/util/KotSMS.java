package com.indogo.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import com.lionpig.webui.http.struct.FunctionItem;
import com.lionpig.webui.http.util.C;

public class KotSMS implements ISMS {

	@SuppressWarnings({ "unused", "finally" })
	private boolean exec_send(FunctionItem fi, String app_phone_no, String message) {
		boolean status = true;
		try {
			String account = fi.getConnection().getGlobalConfig(C.kotsms, C.account);
			String password = fi.getConnection().getGlobalConfig(C.kotsms, C.password);
			String send_message = URLEncoder.encode(message, "UTF-8");
			String url = "http://api.kotsms.com.tw/kotsmsapi-1.php?username=" + account + "&password=" + password + "&dstaddr=" + app_phone_no + "&smbody=" + send_message;
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(url);
			HttpResponse response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			try {
				StringBuffer result = new StringBuffer();
				String line;
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}
			} finally {
				rd.close();
			}
		} catch (Exception ignore) {
			status = false;
		} finally {
			return status;
		}
	}
	
	@SuppressWarnings("finally")
	public boolean send(FunctionItem fi, String app_phone_no) {
		boolean status = false;
		try {
			String message = URLEncoder.encode(fi.getConnection().getGlobalConfig(C.kotsms, C.register_complete_message), "UTF-8");
			status = this.exec_send(fi, app_phone_no, message);
		} catch (Exception e) {
			status = false;
		} finally {
			return status;
		}
	}
	
	public boolean send(FunctionItem fi, String app_phone_no, String message) {
		return this.exec_send(fi, app_phone_no, message);
	}
}
